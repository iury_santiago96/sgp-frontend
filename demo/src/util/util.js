export const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

export const getHeader = () => ({
    headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        Authorization: "Bearer " + sessionStorage.getItem("jhi-authenticationToken")
    }
});
