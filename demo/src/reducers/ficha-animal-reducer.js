import { FAILURE, REQUEST, SUCCESS } from "./action-type";
import { base_url } from "../util/conexao";
import axios from "axios";
import { getHeader } from "../util/util";

const actionName = "FICHA-ANIMAL";
const routeApi = `${base_url}/api/ficha-animal`;

export const ACTION_TYPES = {
  LIMPAR_MENSAGENS: `${actionName}/LIMPAR_MENSAGENS`,
  CLEAN_REGISTER: `${actionName}/CLEAN_REGISTER`,
  CREATE: `${actionName}/CREATE`,
  SEARCH: `${actionName}/SEARCH`,
  UPDATE: `${actionName}/UPDATE`,
  GET: `${actionName}/GET`,
  LIST: `${actionName}/LIST`,
  LISTALL: `${actionName}/LISTALL`,
  LISTREL: `${actionName}/LISTREL`,
  DELETE: `${actionName}/DELETE`
};

const initialState = {
  loading: false,
  fichaAnimal: {},
  fichaAnimals: {},
  fichaAnimalsAll: [],
  errorMessage: {},
  message: {}
};

// Reducer

export default (state = initialState, action) => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.CREATE):
    case REQUEST(ACTION_TYPES.UPDATE):
    case REQUEST(ACTION_TYPES.LIST):
    case REQUEST(ACTION_TYPES.LISTREL):
    case REQUEST(ACTION_TYPES.LISTALL):
    case REQUEST(ACTION_TYPES.DELETE):
    case REQUEST(ACTION_TYPES.GET):
    case REQUEST(ACTION_TYPES.SEARCH):
      return {
        ...state,
        loading: true
      };
    case FAILURE(ACTION_TYPES.UPDATE):
    case FAILURE(ACTION_TYPES.LIST):
    case FAILURE(ACTION_TYPES.CREATE):
    case FAILURE(ACTION_TYPES.LISTREL):
    case FAILURE(ACTION_TYPES.LISTALL):
    case FAILURE(ACTION_TYPES.DELETE):
    case FAILURE(ACTION_TYPES.GET):
    case FAILURE(ACTION_TYPES.SEARCH):
      return {
        ...state,
        loading: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.CREATE):
      return {
        ...state,
        loading: false,
        message: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.UPDATE):
      return {
        ...state,
        loading: false,
        message: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.LISTREL):
    case SUCCESS(ACTION_TYPES.LISTALL):
      return {
        ...state,
        loading: false,
        fichaAnimalsAll: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.SEARCH):
    case SUCCESS(ACTION_TYPES.LIST):
      return {
        ...state,
        loading: false,
        fichaAnimals: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.GET):
      return {
        ...state,
        loading: false,
        fichaAnimal: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE):
      return {
        ...state,
        loading: false,
        message: action.payload.data
      };
    case ACTION_TYPES.LIMPAR_MENSAGENS: {
      return {
        ...state,
        message: {},
        errorMessage: {}
      };
    }

    case ACTION_TYPES.CLEAN_REGISTER: {
      return {
        ...state,
        fichaAnimal: {}
      };
    }

    default:
      return state;
  }
};

export const criar = fichaAnimalData => async dispatch =>
  dispatch({
    type: ACTION_TYPES.CREATE,
    payload: axios.post(`${routeApi}`, fichaAnimalData, getHeader())
  });

export const atualizar = fichaAnimalData => async dispatch => {
  let result = await dispatch({
    type: ACTION_TYPES.UPDATE,
    payload: axios.put(`${routeApi}`, fichaAnimalData, getHeader())
  });
  dispatch(listar());
  return result;
};

export const excluir = id => async dispatch => {
  let result = await dispatch({
    type: ACTION_TYPES.DELETE,
    payload: axios.delete(`${routeApi}/${id}`, getHeader())
  });
  dispatch(listar());
  return result;
};

export const listar = (pageNumber = 0) => async dispatch =>
  dispatch({
    type: ACTION_TYPES.LIST,
    payload: axios.get(`${routeApi}?page=${pageNumber}&size=10`, getHeader())
  });

export const listarRel = id => async dispatch =>
  dispatch({
    type: ACTION_TYPES.LISTREL,
    payload: axios.get(`${routeApi}/relatorio/${id}`, getHeader())
  });

export const listarTodos = () => async dispatch =>
  dispatch({
    type: ACTION_TYPES.LISTALL,
    payload: axios.get(`${routeApi}/all`, getHeader())
  });

export const get = id => async dispatch =>
  dispatch({
    type: ACTION_TYPES.GET,
    payload: axios.get(`${routeApi}/${id}`, getHeader())
  });

export const cleanMessages = () => async dispatch =>
  dispatch({
    type: ACTION_TYPES.LIMPAR_MENSAGENS,
    payload: undefined
  });

export const cleanRegister = () => dispatch =>
  dispatch({
    type: ACTION_TYPES.CLEAN_REGISTER
  });

export const searchRegister = (termo, pageNumber = 0) => dispatch =>
  dispatch({
    type: ACTION_TYPES.SEARCH,
    payload: axios.get(
      `${routeApi}/pesquisa/${termo}?page=${pageNumber}&size=10`,
      getHeader()
    )
  });
