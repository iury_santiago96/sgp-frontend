import {FAILURE, REQUEST, SUCCESS} from "./action-type";
import axios from "axios";
import {base_url} from "../util/conexao";

const actionName = "AUTHENTICATION";

export const ACTION_TYPES = {
    LOGIN: `${actionName}/LOGIN`,
    GET_SESSION: `${actionName}/GET_SESSION`,
    LOGOUT: `${actionName}/LOGOUT`,
    CLEAR_AUTH: `${actionName}/CLEAR_AUTH`,
    ERROR_MESSAGE: `${actionName}/ERROR_MESSAGE`
};

const initialState = {
    loading: false,
    isAuthenticated: false,
    loginSuccess: false,
    ativacaoSuccess: undefined,
    retornoAtivacao: {},
    account: {},
    errorMessage: null, // Errors returned from server side
    loginError: false, // Errors returned from server side
    redirectMessage: null,
    showModalLogin: false
};

// Reducer

export default (state = initialState, action) => {
    switch (action.type) {
        case REQUEST(ACTION_TYPES.LOGIN):
        case REQUEST(ACTION_TYPES.GET_SESSION):
            return {
                ...state,
                loading: true
            };
        case FAILURE(ACTION_TYPES.LOGIN):
            return {
                ...state,
                errorMessage: action.payload.response.data,
                loading: false,
                showModalLogin: true,
                isAuthenticated: false,
                loginError: true,
                ativacaoSuccess: false
            };
        case FAILURE(ACTION_TYPES.GET_SESSION):
            return {
                ...state,
                loading: false,
                isAuthenticated: false,
                showModalLogin: true,
                errorMessage: action.payload
            };
        case SUCCESS(ACTION_TYPES.LOGIN):
            return {
                ...state,
                loading: false,
                loginError: false,
                showModalLogin: false,
                loginSuccess: true
            };
        case ACTION_TYPES.LOGOUT:
            return {
                ...initialState,
                showModalLogin: true
            };
        case SUCCESS(ACTION_TYPES.GET_SESSION): {
            const isAuthenticated =
                action.payload && action.payload.data && action.payload.data.id > 0;
            return {
                ...state,
                isAuthenticated,
                loading: false,
                account: action.payload.data
            };
        }
        case ACTION_TYPES.ERROR_MESSAGE:
            return {
                ...initialState,
                showModalLogin: true,
                redirectMessage: action.message
            };
        case ACTION_TYPES.CLEAR_AUTH:
            sessionStorage.removeItem("jhi-authenticationToken");
            return {
                ...state,
                loading: false,
                showModalLogin: true,
                isAuthenticated: false
            };
        default:
            return state;
    }
};

const getHeader = () => ({
    headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        Authorization: "Bearer " + sessionStorage.getItem("jhi-authenticationToken")
    }
});

export const getSession = () => dispatch => {

    return dispatch({
        type: ACTION_TYPES.GET_SESSION,
        payload: axios.get(base_url + "/api/current-user", getHeader())
    });
};

export const login = userData => async (dispatch, getState) => {
    const result = await dispatch({
        type: ACTION_TYPES.LOGIN,
        payload: axios.post(base_url + "/api/authenticate", userData)
    });
    const bearerToken = result.value.data.id_token;
    if (bearerToken) {
        sessionStorage.setItem("jhi-authenticationToken", bearerToken);
    }
    dispatch(getSession());
};

export const logout = () => dispatch =>
    dispatch({
        type: ACTION_TYPES.CLEAR_AUTH,
        payload: null
    });
