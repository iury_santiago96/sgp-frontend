import { combineReducers } from "redux";
import tipoVoluntarioStore from "./tipo-voluntario-reducer";
import tipoVacinaStore from "./tipo-vacina-reducer";
import tipoContaStore from "./tipo-conta-reducer";
import racaStore from "./raca-reducer";
import baiaStore from "./Baia-reducer";
import procedimentoStore from "./procedimento-reducer";
import vacinaStore from "./vacina-reducer";
import voluntarioStore from "./voluntario-reducer";
import contaStore from "./conta-reducer";
import adotanteStore from "./adotante-reducer";
import usuarioStore from "./usuario-reducer";
import doacaoStore from "./doacao-reducer";
import animalStore from "./animal-reducer";
import authenticationStore from "./authentication-reducer";
import adocaoStore from "./adocao-reducer";
import fichaAnimalStore from "./ficha-animal-reducer";

export default combineReducers({
  tipoVoluntarioStore,
  tipoVacinaStore,
  tipoContaStore,
  racaStore,
  baiaStore,
  procedimentoStore,
  vacinaStore,
  voluntarioStore,
  contaStore,
  adotanteStore,
  usuarioStore,
  doacaoStore,
  animalStore,
  authenticationStore,
  adocaoStore,
  fichaAnimalStore
});
