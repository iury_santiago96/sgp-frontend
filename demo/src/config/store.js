import reducer from '../reducers';
import {applyMiddleware, createStore} from 'redux';
import thunk from 'redux-thunk';
import promiseMiddleware from 'redux-promise-middleware';

const initialize = (initialState = {}, middlewares = []) => {
    const store = createStore(reducer, initialState, applyMiddleware(thunk, promiseMiddleware));
    return store;
};

export default initialize;