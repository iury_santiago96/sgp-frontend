import React from "react";
import PropTypes from "prop-types";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";
import AlertList from "../AlertList/AlertList";

export default function DeleteModal(props) {
  const dispatch = useDispatch();

  const remover = async () => {
    try {
      let result = await dispatch(props.deleteReducer(props.idRegistro));
      if (result.action.payload.status === 204) {
        toast.success(
          <AlertList message={result.action.payload.headers["x-app-alert"]} />
        );
        props.toggle();
      }
    } catch (error) {
      toast.error(
        <AlertList message={error.response.headers["x-app-error"]} />
      );
    }
  };

  return (
    <Modal isOpen={props.isOpen} toggle={props.toggle}>
      <ModalHeader toggle={props.toggle}>Confirmação:</ModalHeader>
      <ModalBody>Deseja realmente remover este registro?</ModalBody>
      <ModalFooter>
        <Button color="primary" onClick={remover} className="mr-1">
          Sim
        </Button>
        <Button color="secondary" onClick={props.toggle}>
          Não
        </Button>
      </ModalFooter>
    </Modal>
  );
}

DeleteModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
  deleteReducer: PropTypes.any.isRequired,
  idRegistro: PropTypes.any.isRequired
};
