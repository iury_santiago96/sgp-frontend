import React, { useEffect } from "react";
import * as router from "react-router-dom";
import { Redirect, Route, Switch } from "react-router-dom";
import { Container, Nav, NavItem, Button } from "reactstrap";
import { useDispatch } from "react-redux";
import {
  AppBreadcrumb2 as AppBreadcrumb,
  AppFooter,
  AppHeader,
  AppNavbarBrand,
  AppSidebar,
  AppSidebarFooter,
  AppSidebarForm,
  AppSidebarHeader,
  AppSidebarMinimizer,
  AppSidebarNav2 as AppSidebarNav,
  AppSidebarToggler
} from "../../../../src";
// sidebar nav config
import navigation from "../../_nav.js";
// routes config
import routes from "../../routes.js";

import logoFull from "../../assets/img/brand/SGPLogo.jpg";
import logo from "../../assets/img/brand/SGP.jpg";
import { ToastContainer } from "react-toastify";
import { logout } from "../../reducers/authentication-reducer";

function DefaultLayout(props) {
  const dispatch = useDispatch();
  useEffect(() => {
    if (!sessionStorage.getItem("jhi-authenticationToken")) {
      props.history.push("/login");
    }
  }, [sessionStorage]);

  function logoutApp() {
    sessionStorage.clear();
    dispatch(logout());
    props.history.push("/login");
  }

  return (
    <div className="app">
      <ToastContainer />
      <AppHeader fixed className="d-print-none">
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand
          full={{ src: logoFull, width: 110, height: 49, alt: "SGP Logo" }}
          minimized={{
            src: logo,
            width: 30,
            height: 30,
            alt: "SGP Logo"
          }}
        />
        <AppSidebarToggler className="d-md-down-none mr-auto" display="lg" />
        <Nav className="d-md-down-none" navbar>
          <NavItem className="px-3">
            <Button onClick={logoutApp}>Sair</Button>
          </NavItem>
        </Nav>
      </AppHeader>
      <div className="app-body">
        <AppSidebar fixed display="lg" className="d-print-none">
          <AppSidebarHeader />
          <AppSidebarForm />
          <AppSidebarNav navConfig={navigation} {...props} router={router} />
          <AppSidebarFooter />
          <AppSidebarMinimizer />
        </AppSidebar>
        <main className="main">
          <AppBreadcrumb appRoutes={routes} router={router} className="d-print-none" />
          <Container fluid id="main-container">
            <Switch>
              {routes.map((route, idx) => {
                return route.component ? (
                  <Route
                    key={idx}
                    path={route.path}
                    exact={route.exact}
                    name={route.name}
                    render={props => <route.component {...props} />}
                  />
                ) : null;
              })}
              <Redirect exact from="/" to="/login" />
            </Switch>
          </Container>
        </main>
      </div>
      <AppFooter className="d-print-none">
        <span>SGP - Sistema de gerenciamento de pet &copy; 2019</span>
        <span className="ml-auto">
          Powered by Iury Santiago e Thiago Petermann
        </span>
      </AppFooter>
    </div>
  );
}

export default DefaultLayout;
