import React from "react";

export default function AlertList({message}) {
    return (
        <ul>{message && message.split(",").map(it => <li key={it}>{it}</li>)}</ul>
    );
}
