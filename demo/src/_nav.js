export default {
  items: [
    {
      name: "Dashboard",
      url: "/dashboard",
      icon: "fa fa-bar-chart",
      badge: {
        variant: "info",
        text: "NEW"
      }
    },
    {
      name: "Gerenciar Tipos",
      url: "#",
      icon: "fa fa-book",
      children: [
        {
          name: "Tipo de Vacina",
          url: "/tipo-vacina",
          icon: "fa fa-flask"
        },
        {
          name: "Tipo de Voluntário",
          url: "/tipo-voluntario",
          icon: "fa fa-user-circle-o"
        },
        {
          name: "Tipo de Conta",
          url: "/tipo-conta",
          icon: "fa fa-envelope-open-o"
        }
      ]
    },
    {
      name: "Gerenciar Adoções",
      url: "/adocao",
      icon: "fa fa-book"
    },
    {
      name: "Gerenciar Animais",
      url: "#",
      icon: "fa fa-address-card",
      children: [
        {
          name: "Animal",
          url: "/animal",
          icon: "fa fa-paw"
        },
        {
          name: "Baia",
          url: "/baia",
          icon: "fa fa-home"
        },
        {
          name: "Raça",
          url: "/raca",
          icon: "fa fa-registered"
        }
      ]
    },
    {
      name: "Gerenciar Ficha Animal",
      url: "#",
      icon: "fa fa-list-alt",
      children: [
        {
          name: "Ficha Animal",
          url: "/ficha-animal",
          icon: "fa fa-clipboard"
        },
        {
          name: "Procedimento",
          url: "/procedimento",
          icon: "fa fa-product-hunt"
        },
        {
          name: "Vacina",
          url: "/vacina",
          icon: "fa fa-stethoscope"
        }
      ]
    },
    {
      name: "Gerenciar RH",
      url: "#",
      icon: "fa fa-users",
      children: [
        {
          name: "Adotante",
          url: "/adotante",
          icon: "fa fa-user-secret "
        },
        {
          name: "Usuario",
          url: "/usuario",
          icon: "fa fa-user-plus"
        },
        {
          name: "Voluntario",
          url: "/voluntario",
          icon: "fa fa-user-md"
        }
      ]
    },
    {
      name: "Gerenciar Financeiro",
      url: "#",
      icon: "fa fa-university",
      children: [
        {
          name: "Doação",
          url: "/doacao",
          icon: "fa fa-shopping-basket"
        },
        {
          name: "Conta",
          url: "/conta",
          icon: "fa fa-usd"
        }
      ]
    }
  ]
};
