import Dashboard from "./views/Dashboard/Dashboard.js";
import Full from "./containers/DefaultLayout/DefaultLayout.js";
import TipoVoluntario from "./views/TipoVoluntario/TipoVoluntario.js";
import TipoVoluntarioInsertEdit from "./views/TipoVoluntario/TipoVoluntarioInsertEdit";
import TipoVacina from "./views/TipoVacina/TipoVacina";
import TipoVacinaInsertEdit from "./views/TipoVacina/TipoVacinaInsertEdit";
import TipoConta from "./views/TipoConta/TipoConta";
import TipoContaInsertEdit from "./views/TipoConta/TipoContaInsertEdit";
import Raca from "./views/Raca/Raca";
import RacaInsertEdit from "./views/Raca/RacaInsertEdit";
import Baia from "./views/Baia/Baia";
import BaiaInsertEdit from "./views/Baia/BaiaInsertEdit";
import Procedimento from "./views/Procedimento/Procedimento";
import ProcedimentoInsertEdit from "./views/Procedimento/ProcedimentoInsertEdit";
import Vacina from "./views/Vacina/Vacina";
import VacinaInsertEdit from "./views/Vacina/VacinaInsertEdit";
import Voluntario from "./views/Voluntario/Voluntario";
import VoluntarioInsertEdit from "./views/Voluntario/VoluntarioInsertEdit";
import Conta from "./views/Conta/Conta";
import ContaInsertEdit from "./views/Conta/ContaInsertEdit";
import Adotante from "./views/Adotante/Adotante";
import AdotanteInsertEdit from "./views/Adotante/AdotanteInsertEdit";
import Usuario from "./views/Usuario/Usuario";
import UsuarioInsertEdit from "./views/Usuario/UsuarioInsertEdit";
import Doacao from "./views/Doacao/Doacao";
import DoacaoInsertEdit from "./views/Doacao/DoacaoInsertEdit";
import Animal from "./views/Animal/Animal";
import AnimalInsertEdit from "./views/Animal/AnimalInsertEdit";
import Adocao from "./views/Adocao/Adocao";
import AdocaoInsertEdit from "./views/Adocao/AdocaoInsertEdit";
import FichaAnimal from "./views/FichaAnimal/FichaAnimal";
import FichaAnimalInsertEdit from "./views/FichaAnimal/FichaAnimalInsertEdit";
import RelFichaAnimal from "./views/Animal/RelFichaAnimal";

const routes = [
  { path: "/home", exact: true, name: "Home", component: Full },
  { path: "/dashboard", name: "Dashboard", component: Dashboard },
  {
    path: "/tipo-voluntario",
    exact: true,
    name: "Tipo de Voluntário",
    component: TipoVoluntario
  },
  {
    path: "/tipo-voluntario/add",
    exact: true,
    name: "Cadastrando Tipo de Voluntário",
    component: TipoVoluntarioInsertEdit
  },
  {
    path: "/tipo-voluntario/edit/:id",
    exact: true,
    name: "Editando Tipo de Voluntário",
    component: TipoVoluntarioInsertEdit
  },
  {
    path: "/tipo-vacina",
    exact: true,
    name: "Tipo de Vacina",
    component: TipoVacina
  },
  {
    path: "/tipo-vacina/add",
    exact: true,
    name: "Cadastrando Tipo de Vacina",
    component: TipoVacinaInsertEdit
  },
  {
    path: "/tipo-vacina/edit/:id",
    exact: true,
    name: "Editando Tipo de Vacina",
    component: TipoVacinaInsertEdit
  },
  {
    path: "/tipo-conta",
    exact: true,
    name: "Tipo de Conta",
    component: TipoConta
  },
  {
    path: "/tipo-conta/add",
    exact: true,
    name: "Cadastrando Tipo de Conta",
    component: TipoContaInsertEdit
  },
  {
    path: "/tipo-conta/edit/:id",
    exact: true,
    name: "Editando Tipo de Conta",
    component: TipoContaInsertEdit
  },
  { path: "/raca", exact: true, name: "Raça", component: Raca },
  {
    path: "/raca/add",
    exact: true,
    name: "Cadastrando Raça",
    component: RacaInsertEdit
  },
  {
    path: "/raca/edit/:id",
    exact: true,
    name: "Editando Raça",
    component: RacaInsertEdit
  },
  { path: "/baia", exact: true, name: "Baia", component: Baia },
  {
    path: "/baia/add",
    exact: true,
    name: "Cadastrando Baia",
    component: BaiaInsertEdit
  },
  {
    path: "/baia/edit/:id",
    exact: true,
    name: "Editando Baia",
    component: BaiaInsertEdit
  },
  {
    path: "/procedimento",
    exact: true,
    name: "Procedimento",
    component: Procedimento
  },
  {
    path: "/procedimento/add",
    exact: true,
    name: "Cadastrando Procedimento",
    component: ProcedimentoInsertEdit
  },
  {
    path: "/procedimento/edit/:id",
    exact: true,
    name: "Editando Procedimento",
    component: ProcedimentoInsertEdit
  },
  { path: "/vacina", exact: true, name: "Vacina", component: Vacina },
  {
    path: "/vacina/add",
    exact: true,
    name: "Cadastrando Vacina",
    component: VacinaInsertEdit
  },
  {
    path: "/vacina/edit/:id",
    exact: true,
    name: "Editando Vacina",
    component: VacinaInsertEdit
  },
  {
    path: "/voluntario",
    exact: true,
    name: "Voluntario",
    component: Voluntario
  },
  {
    path: "/voluntario/add",
    exact: true,
    name: "Cadastrando Voluntario",
    component: VoluntarioInsertEdit
  },
  {
    path: "/voluntario/edit/:id",
    exact: true,
    name: "Editando Voluntario",
    component: VoluntarioInsertEdit
  },
  { path: "/conta", exact: true, name: "Conta", component: Conta },
  {
    path: "/conta/add",
    exact: true,
    name: "Cadastrando Conta",
    component: ContaInsertEdit
  },
  {
    path: "/conta/edit/:id",
    exact: true,
    name: "Editando Conta",
    component: ContaInsertEdit
  },
  { path: "/adotante", exact: true, name: "Adotante", component: Adotante },
  {
    path: "/adotante/add",
    exact: true,
    name: "Cadastrando Adotante",
    component: AdotanteInsertEdit
  },
  {
    path: "/adotante/edit/:id",
    exact: true,
    name: "Editando Adotante",
    component: AdotanteInsertEdit
  },
  { path: "/usuario", exact: true, name: "Usuario", component: Usuario },
  {
    path: "/usuario/add",
    exact: true,
    name: "Cadastrando Usuario",
    component: UsuarioInsertEdit
  },
  {
    path: "/usuario/edit/:id",
    exact: true,
    name: "Editando Usuario",
    component: UsuarioInsertEdit
  },
  { path: "/doacao", exact: true, name: "Doação", component: Doacao },
  {
    path: "/doacao/add",
    exact: true,
    name: "Cadastrando Doação",
    component: DoacaoInsertEdit
  },
  {
    path: "/doacao/edit/:id",
    exact: true,
    name: "Editando Doação",
    component: DoacaoInsertEdit
  },
  { path: "/animal", exact: true, name: "Animal", component: Animal },
  {
    path: "/animal/add",
    exact: true,
    name: "Cadastrando Animal",
    component: AnimalInsertEdit
  },
  {
    path: "/animal/edit/:id",
    exact: true,
    name: "Editando Animal",
    component: AnimalInsertEdit
  },
  { path: "/adocao", exact: true, name: "Adoção", component: Adocao },
  {
    path: "/adocao/add",
    exact: true,
    name: "Cadastrando Adoção",
    component: AdocaoInsertEdit
  },
  {
    path: "/adocao/edit/:id",
    exact: true,
    name: "Editando Adoção",
    component: AdocaoInsertEdit
  },
  {
    path: "/ficha-animal",
    exact: true,
    name: "Ficha Animal",
    component: FichaAnimal
  },
  {
    path: "/ficha-animal/add",
    exact: true,
    name: "Cadastrando Ficha Animal",
    component: FichaAnimalInsertEdit
  },
  {
    path: "/ficha-animal/edit/:id",
    exact: true,
    name: "Editando Ficha Animal",
    component: FichaAnimalInsertEdit
  },
  {
    path: "/animal/rel/:id",
    exact: true,
    name: "Relatório de Ficha Animal",
    component: RelFichaAnimal
  }
];
export default routes;
