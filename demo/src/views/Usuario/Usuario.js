import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import {
  listar,
  searchRegister,
  excluir
} from "../../reducers/usuario-reducer";
import DeleteModal from "../../containers/DeleteModal/DeleteModal";
import { Card, CardFooter, CardHeader } from "reactstrap";
import Pagination from "react-js-pagination";
import * as lodash from "lodash";

const usuario = props => {
  const RotaPagInsert = "/usuario/add";
  const dispatch = useDispatch();
  const [termo, setTermo] = useState("");
  const [usuariosId, setusuariosId] = useState(false);
  const [modalDeleteVisible, setModalDeleteVisible] = useState(false);
  const { loading, usuarios } = useSelector(state => state.usuarioStore);

  useEffect(() => {
    dispatch(listar());
  }, []);

  const onChangeSearchInput = evt => {
    debouncedSearch(evt.target.value);
  };

  const debouncedSearch = lodash.debounce(function(query) {
    setTermo(query);
    if (query && query !== "") {
      dispatch(searchRegister(query));
    } else {
      dispatch(listar());
    }
  }, 500);

  const modalDeleteVisibleToggle = () => {
    setModalDeleteVisible(!modalDeleteVisible);
  };

  const openModalDelete = (id, e) => {
    setusuariosId(id);
    setModalDeleteVisible(true);
  };

  function handlePageChange(pageNumber) {
    if (termo && termo !== "") {
      dispatch(searchRegister(termo, pageNumber - 1));
    } else {
      dispatch(listar(pageNumber - 1));
    }
  }

  const cardHeader = () => {
    return (
      <CardHeader>
        <nav className="navbar navbar-light bg-light">
          <form className="form-inline w-75">
            <input
              className="form-control mr-sm-2 w-25"
              onChange={onChangeSearchInput}
              type="search"
              placeholder="Procurar..."
              aria-label="Search"
            />
          </form>
          <Link to={RotaPagInsert}>
            <button type="button" className="btn btn-primary float-right">
              Inserir
            </button>
          </Link>
          {loading && (
            <div className="d-inline p-2 bg-primary text-white">
              <span>Carregando....</span>
            </div>
          )}
        </nav>
      </CardHeader>
    );
  };

  const elementoTipoVol = tipo => {
    const rotaPagEditView = `/usuario/edit/${tipo.id}`;
    return (
      <tr key={tipo.id} className="text-center">
        <td>{tipo.login}</td>
        <td>***********</td>
        <td>{tipo.voluntario !== null ? tipo.voluntario.nome : undefined}</td>
        <td>
          <Link to={rotaPagEditView}>
            <button type="button" className="btn btn-primary mr-1">
              Visualizar
            </button>
          </Link>
          <button
            type="button"
            className="btn btn-danger"
            onClick={openModalDelete.bind(null, tipo.id)}
          >
            Deletar
          </button>
        </td>
      </tr>
    );
  };

  const cardFooter = () => {
    return (
      <CardFooter>
        <Pagination
          innerClass="pagination justify-content-center"
          itemClass="page-item"
          linkClass="page-link"
          activePage={usuarios.number + 1}
          itemsCountPerPage={usuarios.pageable.pageSize}
          totalItemsCount={usuarios.totalElements}
          onChange={handlePageChange.bind(this)}
        />
      </CardFooter>
    );
  };

  return (
    <div>
      <Card>
        {cardHeader()}
        <table className="table table-striped mb-0 ">
          <thead>
            <tr className="text-center">
              <th scope="col">Login</th>
              <th scope="col">Senha</th>
              <th scope="col">Voluntario</th>
              <th scope="col">Ações</th>
            </tr>
          </thead>
          <tbody>
            {usuarios &&
              usuarios.content &&
              usuarios.content.map(it => elementoTipoVol(it))}
          </tbody>
        </table>
        {usuarios && usuarios.content && cardFooter(usuarios.pageable.number)}
        <DeleteModal
          isOpen={modalDeleteVisible}
          toggle={modalDeleteVisibleToggle}
          idRegistro={usuariosId}
          deleteReducer={excluir}
        />
      </Card>
    </div>
  );
};

export default usuario;
