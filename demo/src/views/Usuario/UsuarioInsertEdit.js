import React, { useEffect, useState } from "react";
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Form,
  FormFeedback,
  Input,
  InputGroup,
  Row
} from "reactstrap";
import { Field, Form as FinalForm } from "react-final-form";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import {
  atualizar,
  cleanMessages,
  cleanRegister,
  criar,
  get
} from "../../reducers/usuario-reducer";
import { listarTodos } from "../../reducers/voluntario-reducer";
import * as Yup from "yup";
import { validate } from "../../util/validation-util";
import { toast } from "react-toastify";
import AlertList from "../../containers/AlertList/AlertList";

const TipoContaInsertEdit = props => {
  const rotaPagInicial = "/usuario";
  const [formValues, setFormValues] = useState(undefined);
  const [isEnabled, setEnabled] = useState(false);
  const dispatch = useDispatch();
  const { usuario } = useSelector(store => store.usuarioStore);
  const { voluntariosAll } = useSelector(store => store.voluntarioStore);

  const esquemaValidacao = Yup.object().shape({
    login: Yup.string()
      .min(5, "O login deve ter mais de 5 caracteres.")
      .required("O login é obrigatório."),
    senha: Yup.string()
      .min(4, "A senha deve ter mais de 4 caracteres.")
      .required("A senha é obrigatória."),
    voluntario: Yup.object({
      id: Yup.string().required("O voluntário é obrigatório.")
    })
  });

  useEffect(() => {
    setFormValues(usuario);
  }, [usuario]);

  useEffect(() => {
    dispatch(listarTodos());
    if (props.match.params && props.match.params.id) {
      dispatch(get(props.match.params.id));
    } else {
      dispatch(cleanRegister());
      setEnabled(true);
    }
    dispatch(cleanMessages());
  }, []);

  const toggleEnabled = () => {
    setEnabled(!isEnabled);
  };

  const submitForm = async values => {
    try {
      setFormValues(values);
      let isUpdate = values.id > 0;
      let result = isUpdate
        ? await dispatch(atualizar(values))
        : await dispatch(criar(values));
      if (
        result.action.payload.status === 200 ||
        result.action.payload.status === 201
      ) {
        props.history.push(rotaPagInicial);
        toast.success(
          <AlertList message={result.action.payload.headers["x-app-alert"]} />
        );
      }
    } catch (error) {
      toast.error(
        <AlertList message={error.response.headers["x-app-error"]} />
      );
    }
  };

  const footer = (
    <CardFooter>
      {isEnabled && (
        <React.Fragment>
          <Button color="primary">Salvar</Button>
          <Link to={rotaPagInicial} className="ml-1">
            <Button>Cancelar</Button>
          </Link>
        </React.Fragment>
      )}
      {!isEnabled && (
        <React.Fragment>
          <Button color="primary" onClick={toggleEnabled}>
            Editar
          </Button>
          <Link to={rotaPagInicial} className="ml-1">
            <Button>Voltar</Button>
          </Link>
        </React.Fragment>
      )}
    </CardFooter>
  );

  const Formulario = () => (
    <FinalForm
      onSubmit={submitForm}
      initialValues={formValues}
      validate={values => validate(values, esquemaValidacao)}
      render={({ handleSubmit, pristine, submitForm, values, meta }) => (
        <Form onSubmit={handleSubmit} name="simpleForm">
          <Field name="id" type="hidden">
            {({ input, meta }) => <Input {...input} />}
          </Field>
          <CardBody>
            <Row>
              <Col>
                <Field name="login">
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">Login</span>
                      <Input
                        type="text"
                        {...input}
                        placeholder="Login do voluntário"
                        invalid={meta.invalid && meta.touched}
                        disabled={!isEnabled}
                      />
                      <FormFeedback>{meta.error}</FormFeedback>
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field type="password" name="senha">
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">Senha</span>
                      <Input
                        type="password"
                        {...input}
                        placeholder="Senha do Voluntario"
                        invalid={meta.invalid && meta.touched}
                        disabled={!isEnabled}
                      />
                      <FormFeedback>{meta.error}</FormFeedback>
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field type="select" name="voluntario.id" component>
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">Voluntario</span>
                      <Input
                        type="select"
                        {...input}
                        disabled={!isEnabled}
                        invalid={meta.invalid && meta.touched}
                      >
                        <option>Escolha um Voluntario...</option>
                        {voluntariosAll &&
                          voluntariosAll.map(it => (
                            <option key={`opt-${it.id}`} value={it.id}>
                              {it.nome}
                            </option>
                          ))}
                      </Input>
                      <FormFeedback>{meta.error}</FormFeedback>
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
          </CardBody>
          {footer}
        </Form>
      )}
    />
  );

  return (
    <div>
      <Row>
        <Col sm="6" className="m-auto">
          <Card>
            <CardHeader>Usuário</CardHeader>
            <Formulario />
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default TipoContaInsertEdit;
