import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import {
  listar,
  searchRegister,
  excluir
} from "../../reducers/ficha-animal-reducer";
import DeleteModal from "../../containers/DeleteModal/DeleteModal";
import { Card, CardFooter, CardHeader } from "reactstrap";
import Pagination from "react-js-pagination";
import * as lodash from "lodash";
import moment from "moment";

const fichaAnimal = props => {
  const RotaPagInsert = "/ficha-animal/add";
  const dispatch = useDispatch();
  const [termo, setTermo] = useState("");
  const [fichaAnimalsId, setfichaAnimalsId] = useState(false);
  const [modalDeleteVisible, setModalDeleteVisible] = useState(false);
  const { loading, fichaAnimals } = useSelector(
    state => state.fichaAnimalStore
  );

  useEffect(() => {
    dispatch(listar());
  }, []);

  const onChangeSearchInput = evt => {
    debouncedSearch(evt.target.value);
  };

  const debouncedSearch = lodash.debounce(function(query) {
    setTermo(query);
    if (query && query !== "") {
      dispatch(searchRegister(query));
    } else {
      dispatch(listar());
    }
  }, 500);

  const modalDeleteVisibleToggle = () => {
    setModalDeleteVisible(!modalDeleteVisible);
  };

  const openModalDelete = (id, e) => {
    setfichaAnimalsId(id);
    setModalDeleteVisible(true);
  };

  function handlePageChange(pageNumber) {
    if (termo && termo !== "") {
      dispatch(searchRegister(termo, pageNumber - 1));
    } else {
      dispatch(listar(pageNumber - 1));
    }
  }

  const cardHeader = () => {
    return (
      <CardHeader>
        <nav className="navbar navbar-light bg-light">
          <form className="form-inline w-75">
            <input
              className="form-control mr-sm-2 w-25"
              onChange={onChangeSearchInput}
              type="search"
              placeholder="Procurar..."
              aria-label="Search"
            />
          </form>
          <Link to={RotaPagInsert}>
            <button type="button" className="btn btn-primary float-right">
              Inserir
            </button>
          </Link>
          {loading && (
            <div className="d-inline p-2 bg-primary text-white">
              <span>Carregando....</span>
            </div>
          )}
        </nav>
      </CardHeader>
    );
  };

  const elementoTipoVol = tipo => {
    const rotaPagEditView = `/ficha-animal/edit/${tipo.id}`;
    return (
      <tr key={tipo.id} className="text-center">
        <td>{moment(tipo.data).format("DD/MM/YYYY")}</td>
        <td>{tipo.observacao}</td>
        <td>{tipo.animal.nome}</td>
        <td>{tipo.voluntario.nome}</td>
        <td>
          <Link to={rotaPagEditView}>
            <button type="button" className="btn btn-primary mr-1">
              Visualizar
            </button>
          </Link>
          <button
            type="button"
            className="btn btn-danger"
            onClick={openModalDelete.bind(null, tipo.id)}
          >
            Deletar
          </button>
        </td>
      </tr>
    );
  };

  const cardFooter = () => {
    return (
      <CardFooter>
        <Pagination
          innerClass="pagination justify-content-center"
          itemClass="page-item"
          linkClass="page-link"
          activePage={fichaAnimals.number + 1}
          itemsCountPerPage={fichaAnimals.pageable.pageSize}
          totalItemsCount={fichaAnimals.totalElements}
          onChange={handlePageChange.bind(this)}
        />
      </CardFooter>
    );
  };

  return (
    <div>
      <Card>
        {cardHeader()}
        <table className="table table-striped mb-0 ">
          <thead>
            <tr className="text-center">
              <th scope="col">Data da Ficha</th>
              <th scope="col">Observação</th>
              <th scope="col">Animal</th>
              <th scope="col">Voluntario</th>
              <th scope="col">Ações</th>
            </tr>
          </thead>
          <tbody>
            {fichaAnimals &&
              fichaAnimals.content &&
              fichaAnimals.content.map(it => elementoTipoVol(it))}
          </tbody>
        </table>
        {fichaAnimals &&
          fichaAnimals.content &&
          cardFooter(fichaAnimals.pageable.number)}
        <DeleteModal
          isOpen={modalDeleteVisible}
          toggle={modalDeleteVisibleToggle}
          idRegistro={fichaAnimalsId}
          deleteReducer={excluir}
        />
      </Card>
    </div>
  );
};

export default fichaAnimal;
