import React, { useEffect, useState } from "react";
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  FormFeedback,
  Col,
  Form,
  Input,
  InputGroup,
  Row
} from "reactstrap";
import { Field, Form as FinalForm } from "react-final-form";
import arrayMutators from "final-form-arrays";
import { FieldArray } from "react-final-form-arrays";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import {
  atualizar,
  cleanMessages,
  cleanRegister,
  criar,
  get
} from "../../reducers/ficha-animal-reducer";
import { listarTodos } from "../../reducers/voluntario-reducer";
import { listarTodos as listAnimalAll } from "../../reducers/animal-reducer";
import { listarTodos as listProcedimentoAll } from "../../reducers/procedimento-reducer";
import { listarTodos as listVacinaAll } from "../../reducers/vacina-reducer";
import * as Yup from "yup";
import { validate } from "../../util/validation-util";
import moment from "moment";
import { toast } from "react-toastify";
import AlertList from "../../containers/AlertList/AlertList";

const TipoContaInsertEdit = props => {
  const rotaPagInicial = "/ficha-animal";
  const [formValues, setFormValues] = useState(undefined);
  const [isEnabled, setEnabled] = useState(false);
  const dispatch = useDispatch();
  const { fichaAnimal } = useSelector(store => store.fichaAnimalStore);
  const { voluntariosAll } = useSelector(store => store.voluntarioStore);
  const { animalsAll } = useSelector(store => store.animalStore);
  const { procedimentosAll } = useSelector(store => store.procedimentoStore);
  const { vacinasAll } = useSelector(store => store.vacinaStore);

  const esquemaValidacao = Yup.object({
    data: Yup.date().required("A data é necessária."),
    voluntario: Yup.object({
      id: Yup.string().required("O voluntário é necessário.")
    }),
    animal: Yup.object({
      id: Yup.string().required("O animal é necessário.")
    })
  });

  useEffect(() => {
    let dto = Object.assign({}, fichaAnimal);
    dto.data =
      fichaAnimal.data !== undefined
        ? moment(fichaAnimal.data).format("YYYY-MM-DD")
        : undefined;
    setFormValues(dto);
  }, [fichaAnimal]);

  useEffect(() => {
    dispatch(listarTodos());
    dispatch(listAnimalAll());
    dispatch(listProcedimentoAll());
    dispatch(listVacinaAll());
    if (props.match.params && props.match.params.id) {
      dispatch(get(props.match.params.id));
    } else {
      dispatch(cleanRegister());
      setEnabled(true);
    }
    dispatch(cleanMessages());
  }, []);

  const toggleEnabled = () => {
    setEnabled(!isEnabled);
  };

  const submitForm = async values => {
    try {
      let dto = Object.assign({}, values);
      dto.data =
        values.data !== null ? moment(values.data, moment.ISO_8601) : null;
      setFormValues(dto);
      let isUpdate = dto.id > 0;
      let result = isUpdate
        ? await dispatch(atualizar(dto))
        : await dispatch(criar(dto));
      if (
        result.action.payload.status === 200 ||
        result.action.payload.status === 201
      ) {
        props.history.push(rotaPagInicial);
        toast.success(
          <AlertList message={result.action.payload.headers["x-app-alert"]} />
        );
      }
    } catch (error) {
      toast.error(
        <AlertList message={error.response.headers["x-app-error"]} />
      );
    }
  };

  const footer = (
    <CardFooter>
      {isEnabled && (
        <React.Fragment>
          <Button color="primary">Salvar</Button>
          <Link to={rotaPagInicial} className="ml-1">
            <Button>Cancelar</Button>
          </Link>
        </React.Fragment>
      )}
      {!isEnabled && (
        <React.Fragment>
          <Button color="primary" onClick={toggleEnabled}>
            Editar
          </Button>
          <Link to={rotaPagInicial} className="ml-1">
            <Button>Voltar</Button>
          </Link>
        </React.Fragment>
      )}
    </CardFooter>
  );

  const Formulario = () => (
    <FinalForm
      onSubmit={submitForm}
      initialValues={formValues}
      validate={values => validate(values, esquemaValidacao)}
      mutators={{
        ...arrayMutators
      }}
      render={({
        handleSubmit,
        pristine,
        submitForm,
        values,
        meta,
        form: {
          mutators: { push, pop }
        }
      }) => (
        <Form onSubmit={handleSubmit} name="simpleForm">
          <Field name="id" type="hidden">
            {({ input, meta }) => <Input {...input} />}
          </Field>
          <CardBody>
            <Row>
              <Col>
                <Field type="date" name="data">
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">Data da ficha</span>
                      <Input
                        type="date"
                        {...input}
                        invalid={meta.invalid && meta.touched}
                        disabled={!isEnabled}
                      />
                      <FormFeedback>{meta.error}</FormFeedback>
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field type="select" name="voluntario.id" component>
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">Voluntário</span>
                      <Input
                        type="select"
                        {...input}
                        invalid={meta.invalid && meta.touched}
                        disabled={!isEnabled}
                      >
                        <option>Escolha uma voluntário...</option>
                        {voluntariosAll &&
                          voluntariosAll.map(it => (
                            <option key={`opt-${it.id}`} value={it.id}>
                              {it.nome}
                            </option>
                          ))}
                      </Input>
                      <FormFeedback>{meta.error}</FormFeedback>
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field type="select" name="animal.id" component>
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">Animal</span>
                      <Input
                        type="select"
                        {...input}
                        invalid={meta.invalid && meta.touched}
                        disabled={!isEnabled}
                      >
                        <option>Escolha um Animal...</option>
                        {animalsAll &&
                          animalsAll.map(it => (
                            <option key={`opt-${it.id}`} value={it.id}>
                              {it.nome}
                            </option>
                          ))}
                      </Input>
                      <FormFeedback>{meta.error}</FormFeedback>
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field name="observacao">
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">Observação</span>
                      <textarea
                        className="form-control"
                        rows="3"
                        {...input}
                        placeholder="Observação da ficha"
                        disabled={!isEnabled}
                      />
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
            <hr />
            <div className="d-flex justify-content-center mt-2 mb-3">
              <button
                className="btn btn-success mr-1"
                type="button"
                disabled={!isEnabled}
                onClick={() => push("vacinas", undefined)}
              >
                Adicionar Vacina
              </button>
              <button
                className="btn btn-danger"
                type="button"
                disabled={!isEnabled}
                onClick={() => pop("vacinas")}
              >
                Remover Vacina
              </button>
            </div>
            <Row>
              <Col>
                <div>
                  <FieldArray name="vacinas">
                    {({ fields }) =>
                      fields.map((name, index) => (
                        <div key={name}>
                          <Row>
                            <label hidden>Vacina. #{index + 1}</label>
                            <Col>
                              <Field
                                type="select"
                                name={`${name}.id`}
                                component
                              >
                                {({ input, meta }) => (
                                  <InputGroup className="mb-2">
                                    <span className="input-group-text">
                                      Vacina {index + 1}
                                    </span>
                                    <Input
                                      type="select"
                                      {...input}
                                      invalid={meta.invalid && meta.touched}
                                      disabled={!isEnabled}
                                    >
                                      <option>Escolha uma Vacina...</option>
                                      {vacinasAll &&
                                        vacinasAll.map(it => (
                                          <option
                                            key={`opt-${it.id}`}
                                            value={it.id}
                                          >
                                            {it.nome}
                                          </option>
                                        ))}
                                    </Input>
                                    <div className="input-group-append">
                                      <button
                                        className="btn btn-outline-secondary"
                                        type="button"
                                        disabled={!isEnabled}
                                        onClick={() => fields.remove(index)}
                                      >
                                        ❌
                                      </button>
                                    </div>
                                    <FormFeedback>{meta.error}</FormFeedback>
                                  </InputGroup>
                                )}
                              </Field>
                            </Col>
                          </Row>
                        </div>
                      ))
                    }
                  </FieldArray>
                </div>
              </Col>
            </Row>
            <hr />
            <div className="d-flex justify-content-center mt-2 mb-3">
              <button
                className="btn btn-success mr-1"
                type="button"
                disabled={!isEnabled}
                onClick={() => push("procedimentos", undefined)}
              >
                Adicionar Procedimento
              </button>
              <button
                className="btn btn-danger"
                type="button"
                disabled={!isEnabled}
                onClick={() => pop("procedimentos")}
              >
                Remover Procedimento
              </button>
            </div>
            <Row>
              <Col>
                <div>
                  <FieldArray name="procedimentos">
                    {({ fields }) =>
                      fields.map((name, index) => (
                        <div key={name}>
                          <Row>
                            <label hidden>Procedimento. #{index + 1}</label>
                            <Col>
                              <Field
                                type="select"
                                name={`${name}.id`}
                                component
                              >
                                {({ input, meta }) => (
                                  <InputGroup className="mb-2">
                                    <span className="input-group-text">
                                      Procedimento {index + 1}
                                    </span>
                                    <Input
                                      type="select"
                                      {...input}
                                      disabled={!isEnabled}
                                    >
                                      <option>
                                        Escolha um Procedimento...
                                      </option>
                                      {procedimentosAll &&
                                        procedimentosAll.map(it => (
                                          <option
                                            key={`opt-${it.id}`}
                                            value={it.id}
                                          >
                                            {it.tipo}
                                          </option>
                                        ))}
                                    </Input>
                                    <div className="input-group-append">
                                      <button
                                        className="btn btn-outline-secondary"
                                        type="button"
                                        disabled={!isEnabled}
                                        onClick={() => fields.remove(index)}
                                      >
                                        ❌
                                      </button>
                                    </div>
                                  </InputGroup>
                                )}
                              </Field>
                            </Col>
                          </Row>
                        </div>
                      ))
                    }
                  </FieldArray>
                </div>
              </Col>
            </Row>
            <hr />
          </CardBody>
          {footer}
        </Form>
      )}
    />
  );

  return (
    <div>
      <Row>
        <Col sm="6" className="m-auto">
          <Card>
            <CardHeader>Ficha Animal</CardHeader>
            <Formulario />
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default TipoContaInsertEdit;
