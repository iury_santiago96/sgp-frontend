import React, { useEffect } from "react";
import { Formik } from "formik";
import { login } from "../../reducers/authentication-reducer";
import { useDispatch, useSelector } from "react-redux";
import {
  Button,
  Card,
  CardBody,
  CardGroup,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row
} from "reactstrap";
import * as Yup from "yup";

const Login = props => {
  const dispatch = useDispatch();
  const { isAuthenticated, loginSuccess, loginError } = useSelector(
    state => state.authenticationStore
  );

  useEffect(() => {
    if (loginSuccess && isAuthenticated) {
      props.history.push("/dashboard");
    }
  }, [isAuthenticated, loginSuccess]);

  const validate = getValidationSchema => {
    return values => {
      const validationSchema = getValidationSchema(values);
      try {
        validationSchema.validateSync(values, { abortEarly: false });
        return {};
      } catch (error) {
        return getErrorsFromValidationError(error);
      }
    };
  };

  const getErrorsFromValidationError = validationError => {
    const FIRST_ERROR = 0;
    return validationError.inner.reduce((errors, error) => {
      return {
        ...errors,
        [error.path]: error.errors[FIRST_ERROR]
      };
    }, {});
  };

  const validationSchema = function(values) {
    return Yup.object().shape({
      username: Yup.string()
        .min(5, `O login deve ter ao menos 5 caracteres`)
        .required("Login é obrigatório"),
      password: Yup.string()
        .min(4, `A senha deve ter ao menos ${4} caracteres!`)
        .required("A senha é obrigatório")
    });
  };

  const onSubmit = (values, { setSubmitting, setErrors }) => {
    dispatch(login(values));
    setSubmitting(false);
  };

  const valoresIniciais = {
    username: "",
    password: "",
    rememberMe: false
  };

  const loginAlert = (
    <div className="alert alert-danger" role="alert">
      Usuário ou senha incorretos.
    </div>
  );

  return (
    <div className="app flex-row align-items-center">
      <Container>
        <Row className="justify-content-center">
          <Col md="8">{loginError && loginAlert}</Col>
          <Col md="8">
            <CardGroup>
              <Card className="p-4">
                <CardBody>
                  <Formik
                    initialValues={valoresIniciais}
                    validate={validate(validationSchema)}
                    onSubmit={onSubmit}
                    render={({
                      handleSubmit,
                      values,
                      handleChange,
                      errors
                    }) => (
                      <Form onSubmit={handleSubmit} name="simpleForm">
                        <h1>Login</h1>
                        <p className="text-muted">Informe suas credenciais</p>
                        <InputGroup className="mb-3">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-user">Login</i>
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="text"
                            id="username"
                            value={values.username}
                            placeholder="Login"
                            invalid={!!errors.username}
                            autoComplete="username"
                            onChange={handleChange}
                            valid={!errors.username}
                          />
                        </InputGroup>
                        <InputGroup>
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-lock">Senha</i>
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="password"
                            id="password"
                            value={values.password}
                            placeholder="Senha"
                            autoComplete="current-password"
                            onChange={handleChange}
                            valid={!errors.password}
                            invalid={!!errors.password}
                          />
                        </InputGroup>

                        <Row className="mt-4">
                          <Col className="col-xs-6">
                            <Button
                              color="primary"
                              className="px-4"
                              type="submit"
                              block
                            >
                              Entrar
                            </Button>
                          </Col>
                        </Row>
                      </Form>
                    )}
                  />
                </CardBody>
              </Card>
            </CardGroup>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Login;
