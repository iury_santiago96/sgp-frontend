import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import {
  listar,
  searchRegister,
  excluir
} from "../../reducers/voluntario-reducer";
import DeleteModal from "../../containers/DeleteModal/DeleteModal";
import { Card, CardFooter, CardHeader } from "reactstrap";
import Pagination from "react-js-pagination";
import * as lodash from "lodash";

const voluntario = props => {
  const RotaPagInsert = "/voluntario/add";
  const dispatch = useDispatch();
  const [termo, setTermo] = useState("");
  const [voluntariosId, setvoluntariosId] = useState(false);
  const [modalDeleteVisible, setModalDeleteVisible] = useState(false);
  const { loading, voluntarios } = useSelector(state => state.voluntarioStore);

  useEffect(() => {
    dispatch(listar());
  }, []);

  const onChangeSearchInput = evt => {
    debouncedSearch(evt.target.value);
  };

  const debouncedSearch = lodash.debounce(function(query) {
    setTermo(query);
    if (query && query !== "") {
      dispatch(searchRegister(query));
    } else {
      dispatch(listar());
    }
  }, 500);

  const modalDeleteVisibleToggle = () => {
    setModalDeleteVisible(!modalDeleteVisible);
  };

  const openModalDelete = (id, e) => {
    setvoluntariosId(id);
    setModalDeleteVisible(true);
  };

  function handlePageChange(pageNumber) {
    if (termo && termo !== "") {
      dispatch(searchRegister(termo, pageNumber - 1));
    } else {
      dispatch(listar(pageNumber - 1));
    }
  }

  const cardHeader = () => {
    return (
      <CardHeader>
        <nav className="navbar navbar-light bg-light">
          <form className="form-inline w-75">
            <input
              className="form-control mr-sm-2 w-25"
              onChange={onChangeSearchInput}
              type="search"
              placeholder="Procurar..."
              aria-label="Search"
            />
          </form>
          <Link to={RotaPagInsert}>
            <button type="button" className="btn btn-primary float-right">
              Inserir
            </button>
          </Link>
          {loading && (
            <div className="d-inline p-2 bg-primary text-white">
              <span>Carregando....</span>
            </div>
          )}
        </nav>
      </CardHeader>
    );
  };

  const elementoTipoVol = tipo => {
    const rotaPagEditView = `/voluntario/edit/${tipo.id}`;
    return (
      <tr key={tipo.id} className="text-center">
        <td>{tipo.nome}</td>
        <td>{tipo.rg}</td>
        <td>{tipo.cpf}</td>
        <td>{tipo.telefone}</td>
        <td>{tipo.tipoVoluntario.tipo}</td>
        <td>
          <Link to={rotaPagEditView}>
            <button type="button" className="btn btn-primary mr-1">
              Visualizar
            </button>
          </Link>
          <button
            type="button"
            className="btn btn-danger"
            onClick={openModalDelete.bind(null, tipo.id)}
          >
            Deletar
          </button>
        </td>
      </tr>
    );
  };

  const cardFooter = () => {
    return (
      <CardFooter>
        <Pagination
          innerClass="pagination justify-content-center"
          itemClass="page-item"
          linkClass="page-link"
          activePage={voluntarios.number + 1}
          itemsCountPerPage={voluntarios.pageable.pageSize}
          totalItemsCount={voluntarios.totalElements}
          onChange={handlePageChange.bind(this)}
        />
      </CardFooter>
    );
  };

  return (
    <div>
      <Card>
        {cardHeader()}
        <table className="table table-striped mb-0 ">
          <thead>
            <tr className="text-center">
              <th scope="col">Nome</th>
              <th scope="col">RG</th>
              <th scope="col">CPF</th>
              <th scope="col">Telefone</th>
              <th scope="col">Tipo de voluntario</th>
              <th scope="col">Ações</th>
            </tr>
          </thead>
          <tbody>
            {voluntarios &&
              voluntarios.content &&
              voluntarios.content.map(it => elementoTipoVol(it))}
          </tbody>
        </table>
        {voluntarios &&
          voluntarios.content &&
          cardFooter(voluntarios.pageable.number)}
        <DeleteModal
          isOpen={modalDeleteVisible}
          toggle={modalDeleteVisibleToggle}
          idRegistro={voluntariosId}
          deleteReducer={excluir}
        />
      </Card>
    </div>
  );
};

export default voluntario;
