import React, { useEffect, useState } from "react";
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Form,
  FormFeedback,
  Input,
  InputGroup,
  Row
} from "reactstrap";
import { Field, Form as FinalForm } from "react-final-form";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import {
  atualizar,
  cleanMessages,
  cleanRegister,
  criar,
  get
} from "../../reducers/adotante-reducer";
import * as Yup from "yup";
import { validate } from "../../util/validation-util";
import { toast } from "react-toastify";
import AlertList from "../../containers/AlertList/AlertList";
import InputMask from "react-input-mask";

const TipoContaInsertEdit = props => {
  const rotaPagInicial = "/adotante";
  const [formValues, setFormValues] = useState(undefined);
  const [isEnabled, setEnabled] = useState(false);
  const dispatch = useDispatch();
  const { adotante } = useSelector(store => store.adotanteStore);

  const esquemaValidacao = Yup.object({
    nome: Yup.string().required("O nome é obrigatório."),
    rg: Yup.string()
      .min(9, "O rg deve ter no minímo 9 digitos.")
      .required("O rg é obrigatório."),
    cpf: Yup.string()
      .min(11, "O cpf deve ter no minímo 9 digitos.")
      .required("O cpf é obrigatório."),
    telefone: Yup.string()
      .min(11, "O telefone deve ter no minímo 9 digitos.")
      .required("O telefone é obrigatório."),
    endereco: Yup.object({
      rua: Yup.string().required("A rua é obrigatória."),
      numero: Yup.string().required("O numero é obrigatório."),
      bairro: Yup.string().required("O bairro é obrigatório."),
      cidade: Yup.string().required("A cidade é obrigatória."),
      municipio: Yup.string().required("O municipio é obrigatório.")
    })
  });

  useEffect(() => {
    setFormValues(adotante);
  }, [adotante]);

  useEffect(() => {
    if (props.match.params && props.match.params.id) {
      dispatch(get(props.match.params.id));
    } else {
      dispatch(cleanRegister());
      setEnabled(true);
    }
    dispatch(cleanMessages());
  }, []);

  const toggleEnabled = () => {
    setEnabled(!isEnabled);
  };

  const submitForm = async values => {
    try {
      setFormValues(values);
      let isUpdate = values.id > 0;
      let result = isUpdate
        ? await dispatch(atualizar(values))
        : await dispatch(criar(values));
      console.log(result);
      if (
        result.action.payload.status === 200 ||
        result.action.payload.status === 201
      ) {
        props.history.push(rotaPagInicial);
        toast.success(
          <AlertList message={result.action.payload.headers["x-app-alert"]} />
        );
      }
    } catch (error) {
      toast.error(
        <AlertList message={error.response.headers["x-app-error"]} />
      );
    }
  };

  const footer = (
    <CardFooter>
      {isEnabled && (
        <React.Fragment>
          <Button color="primary">Salvar</Button>
          <Link to={rotaPagInicial} className="ml-1">
            <Button>Cancelar</Button>
          </Link>
        </React.Fragment>
      )}
      {!isEnabled && (
        <React.Fragment>
          <Button color="primary" onClick={toggleEnabled}>
            Editar
          </Button>
          <Link to={rotaPagInicial} className="ml-1">
            <Button>Voltar</Button>
          </Link>
        </React.Fragment>
      )}
    </CardFooter>
  );

  const Formulario = () => (
    <FinalForm
      onSubmit={submitForm}
      initialValues={formValues}
      validate={values => validate(values, esquemaValidacao)}
      render={({ handleSubmit, pristine, submitForm, values, meta }) => (
        <Form onSubmit={handleSubmit} name="simpleForm">
          <Field name="id" type="hidden">
            {({ input, meta }) => <Input {...input} />}
          </Field>
          <CardBody>
            <Row>
              <Col>
                <Field name="nome">
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">Nome</span>
                      <Input
                        type="text"
                        {...input}
                        placeholder="nome do adotante"
                        invalid={meta.invalid && meta.touched}
                        disabled={!isEnabled}
                      />
                      <FormFeedback>{meta.error}</FormFeedback>
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field name="rg">
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">RG</span>
                      <InputMask
                        className={`form-control ${meta.invalid &&
                          meta.touched &&
                          "is-invalid"}`}
                        mask="99.999.999"
                        type="text"
                        {...input}
                        alwaysShowMask={true}
                        invalid={toString.call(meta.invalid && meta.touched)}
                        disabled={!isEnabled}
                      ></InputMask>
                      <FormFeedback>{meta.error}</FormFeedback>
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field name="cpf">
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">CPF</span>
                      <InputMask
                        className={`form-control ${meta.invalid &&
                          meta.touched &&
                          "is-invalid"}`}
                        mask="999.999.999-99"
                        type="text"
                        alwaysShowMask={true}
                        {...input}
                        invalid={toString.call(meta.invalid && meta.touched)}
                        disabled={!isEnabled}
                      />
                      <FormFeedback>{meta.error}</FormFeedback>
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field name="telefone">
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">
                        Telefone {console.log(meta.invalid)}
                        {console.log(meta.touched)}
                      </span>
                      <InputMask
                        className={`form-control ${meta.invalid &&
                          meta.touched &&
                          "is-invalid"}`}
                        mask="(99) 9 9999-9999"
                        {...input}
                        alwaysShowMask={true}
                        invalid={toString.call(meta.invalid && meta.touched)}
                        disabled={!isEnabled}
                      />
                      <FormFeedback>{meta.error}</FormFeedback>
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field name="observacao">
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">Observação</span>
                      <textarea
                        className="form-control"
                        rows="3"
                        {...input}
                        placeholder="Observações sobre o adotante"
                        disabled={!isEnabled}
                      />
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
            <hr />
            <Row>
              <Col>
                <Field name="endereco.rua">
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">Rua</span>
                      <Input
                        name="rua"
                        type="text"
                        {...input}
                        placeholder="Rua"
                        invalid={meta.invalid && meta.touched}
                        disabled={!isEnabled}
                      />
                      <FormFeedback>{meta.error}</FormFeedback>
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field name="endereco.numero">
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">Numero</span>
                      <Input
                        name="rua"
                        type="text"
                        {...input}
                        placeholder="Numero"
                        invalid={meta.invalid && meta.touched}
                        disabled={!isEnabled}
                      />
                      <FormFeedback>{meta.error}</FormFeedback>
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field name="endereco.bairro">
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">Bairro</span>
                      <Input
                        name="rua"
                        type="text"
                        {...input}
                        placeholder="Bairro"
                        invalid={meta.invalid && meta.touched}
                        disabled={!isEnabled}
                      />
                      <FormFeedback>{meta.error}</FormFeedback>
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field name="endereco.cidade">
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">Cidade</span>
                      <Input
                        name="rua"
                        type="text"
                        {...input}
                        placeholder="Cidade"
                        invalid={meta.invalid && meta.touched}
                        disabled={!isEnabled}
                      />
                      <FormFeedback>{meta.error}</FormFeedback>
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field name="endereco.municipio">
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">Estado</span>
                      <Input
                        name="rua"
                        type="text"
                        {...input}
                        placeholder="Estado"
                        invalid={meta.invalid && meta.touched}
                        disabled={!isEnabled}
                      />
                      <FormFeedback>{meta.error}</FormFeedback>
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
          </CardBody>
          {footer}
        </Form>
      )}
    />
  );

  return (
    <div>
      <Row>
        <Col sm="6" className="m-auto">
          <Card>
            <CardHeader>Adotante</CardHeader>
            <Formulario />
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default TipoContaInsertEdit;
