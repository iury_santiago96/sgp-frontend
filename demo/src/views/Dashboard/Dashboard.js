import React, { useEffect, useState } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  Table,
  CardGroup,
  Progress
} from "reactstrap";
import { Line } from "react-chartjs-2";
import { CustomTooltips } from "@coreui/coreui-plugin-chartjs-custom-tooltips";
import { getHeader } from "../../util/util";
import axios from "axios";

const api = axios.create({
  baseURL: "http://localhost:3333/api"
});

const options = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips
  },
  maintainAspectRatio: false
};

export default function Dashboard() {
  const [dashboard, setDashboard] = useState({
    totalAnimais: 0,
    totalAnimaisAdotados: 0,
    totalDoacoes: 0,
    totalFichas: 0,
    totalVoluntarios: 0,
    totalAnimaisPorBaia: null,
    topDezAdotantes: null
  });
  const [graficoConta, setGraficoConta] = useState(null);

  useEffect(() => {
    async function getAnimais() {
      const response = await api.get("/dashboard", getHeader());
      setDashboard(response.data);
      setGraficoConta(response.data.graficoConta);
    }

    getAnimais();
  }, []);

  return (
    <div className="animated fadeIn">
      <CardGroup className="mb-4">
        <Card>
          <CardBody>
            <div className="h1 text-muted text-right mb-2">
              <i className="fa fa-paw"></i>
            </div>
            <div className="h4 mb-0">{dashboard.totalAnimais}</div>
            <small className="text-muted text-uppercase font-weight-bold">
              Total de Animais
            </small>
            <Progress color="info" value={dashboard.totalAnimais} />
          </CardBody>
        </Card>

        <Card>
          <CardBody>
            <div className="h1 text-muted text-right mb-2">
              <i className="fa fa-user-md"></i>
            </div>
            <div className="h4 mb-0">{dashboard.totalAnimaisAdotados}</div>
            <small className="text-muted text-uppercase font-weight-bold">
              Total de Animais Adotados
            </small>
            <Progress color="success" value={dashboard.totalAnimaisAdotados} />
          </CardBody>
        </Card>

        <Card>
          <CardBody>
            <div className="h1 text-muted text-right mb-2">
              <i className="fa fa-shopping-basket"></i>
            </div>
            <div className="h4 mb-0">{dashboard.totalDoacoes}</div>
            <small className="text-muted text-uppercase font-weight-bold">
              Total de Doações
            </small>
            <Progress color="warning" value={dashboard.totalDoacoes} />
          </CardBody>
        </Card>

        <Card>
          <CardBody>
            <div className="h1 text-muted text-right mb-2">
              <i className="fa fa-list-alt"></i>
            </div>
            <div className="h4 mb-0">{dashboard.totalFichas}</div>
            <small className="text-muted text-uppercase font-weight-bold">
              Total de Fichas
            </small>
            <Progress color="primary" value={dashboard.totalFichas} />
          </CardBody>
        </Card>

        <Card>
          <CardBody>
            <div className="h1 text-muted text-right mb-2">
              <i className="fa fa-users"></i>
            </div>
            <div className="h4 mb-0">{dashboard.totalVoluntarios}</div>
            <small className="text-muted text-uppercase font-weight-bold">
              Total de Voluntários
            </small>
            <Progress color="danger" value={dashboard.totalVoluntarios} />
          </CardBody>
        </Card>
      </CardGroup>

      <Row>
        <Col xs="12" lg="6">
          <Card>
            <CardHeader>
              <i className="fa fa-align-justify"></i> Total de Animais por Baia
            </CardHeader>
            <CardBody>
              <Table responsive>
                <thead>
                  <tr>
                    <th>Baias</th>
                    <th>Total de Animais/Capacidade</th>
                  </tr>
                </thead>
                <tbody>
                  {dashboard.totalAnimaisPorBaia &&
                    dashboard.totalAnimaisPorBaia.map(it => (
                      <tr key={it.baia}>
                        <td>{it.baia}</td>
                        <td>
                          {it.total} / {it.capacidade}
                        </td>
                      </tr>
                    ))}
                </tbody>
              </Table>
            </CardBody>
          </Card>
        </Col>
        <Col cs="12" lg="6">
          <Card>
            <CardHeader>
              <i className="fa fa-align-justify"></i> Top 10 Adotantes
            </CardHeader>
            <CardBody>
              <Table responsive>
                <thead>
                  <tr>
                    <th>Adotante</th>
                    <th>Total de Animais Adotados</th>
                  </tr>
                </thead>
                <tbody>
                  {dashboard.topDezAdotantes &&
                    dashboard.topDezAdotantes.map(it => (
                      <tr key={it.nome}>
                        <td>{it.nome}</td>
                        <td>{it.total}</td>
                      </tr>
                    ))}
                </tbody>
              </Table>
            </CardBody>
          </Card>
        </Col>
      </Row>

      <Row>
        <Col xs="12">
          <Card>
            <CardHeader>Resumo de Contas</CardHeader>
            <CardBody>
              <div className="chart-wrapper">
                {graficoConta ? (
                  <Line data={graficoConta} options={options} />
                ) : null}
              </div>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  );
}
