import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Card, CardBody, CardFooter, CardHeader, Button, Table } from "reactstrap";
import { Link } from "react-router-dom";
import { listarRel } from "../../reducers/ficha-animal-reducer";
import moment from "moment";

const TipoContaInsertEdit = props => {
  const rotaPagInicial = "/animal";
  const { fichaAnimalsAll } = useSelector(store => store.fichaAnimalStore);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(listarRel(props.match.params.id));
  }, []);

  return (
    <Card className="bg-light report">
      {fichaAnimalsAll &&
        fichaAnimalsAll.map(it => (
          <div key={`card-${it.id}`}>
            <Card className="mb-0">
              <CardHeader>Nº da ficha: {it.id}</CardHeader>
              <CardBody>
                <div key={`ficha-${it.id}`}>
                  <span key={it.id}>
                    <Table responsive>
                      <thead>
                        <tr>
                          <th className="text-center">
                            Data: {moment(it.data).format("DD/MM/YYYY")}
                          </th>
                          <th className="text-center">
                            Animal: {it.animal.nome}
                          </th>
                          <th className="text-center">
                            Voluntário: {it.voluntario.nome}
                          </th>
                        </tr>
                      </thead>
                    </Table>
                  </span>
                  <p className="text-justify bg-light text-dark">
                    <b>Observação:</b> {it.observacao}
                  </p>
                  <br />
                  <h6 className="text-center">Procedimentos</h6>
                  <Table responsive bordered>
                    <thead>
                      <tr>
                        <th className="text-center" scope="col">
                          Procedimento
                        </th>
                        <th className="text-center" scope="col">
                          Descrição
                        </th>
                      </tr>
                    </thead>
                    {it.procedimentos &&
                      it.procedimentos.map(proc => (
                        <tbody key={proc.id}>
                          <tr key={proc.id}>
                            <td className="text-center">{proc.tipo}</td>
                            <td className="text-center">
                              {proc.descricao === null ? undefined : proc.descricao}
                            </td>
                          </tr>
                        </tbody>
                      ))}
                  </Table>
                  <h6 className="text-center">Vacinas</h6>
                  <Table responsive bordered>
                    <thead>
                      <tr>
                        <th className="text-center" scope="col">
                          Vacina
                        </th>
                        <th className="text-center" scope="col">
                          Tipo de Vacina
                        </th>
                        <th className="text-center" scope="col">
                          Descrição
                        </th>
                      </tr>
                    </thead>
                    {it.vacinas &&
                      it.vacinas.map(vac => (
                        <tbody key={vac.id}>
                          <tr key={vac.id}>
                            <td className="text-center">{vac.nome}</td>
                            <td className="text-center">
                              {vac.tipoVacina.tipo}
                            </td>
                            <td className="text-center">
                              {vac.tipoVacina.descricao === null ? undefined : vac.tipoVacina.descricao}
                            </td>
                          </tr>
                        </tbody>
                      ))}
                  </Table>
                </div>
              </CardBody>
            </Card>
          </div>
        ))}
      <CardFooter className="d-print-none">
        <Button color="primary" onClick={() => window.print()}>
          Imprimir
        </Button>
        <Link to={rotaPagInicial} className="ml-1">
          <Button>Cancelar</Button>
        </Link>
      </CardFooter>
    </Card>
  );
};

export default TipoContaInsertEdit;
