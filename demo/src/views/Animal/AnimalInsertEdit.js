import React, { useEffect, useState } from "react";
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Form,
  FormFeedback,
  Input,
  InputGroup,
  Row
} from "reactstrap";
import { Field, Form as FinalForm } from "react-final-form";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import {
  atualizar,
  cleanMessages,
  cleanRegister,
  criar,
  get
} from "../../reducers/animal-reducer";
import { listarTodos } from "../../reducers/Baia-reducer";
import { listarTodos as listRacaAll } from "../../reducers/raca-reducer";
import { listarRel } from "../../reducers/ficha-animal-reducer";
import * as Yup from "yup";
import { validate } from "../../util/validation-util";
import moment from "moment";
import { toast } from "react-toastify";
import AlertList from "../../containers/AlertList/AlertList";

const TipoContaInsertEdit = props => {
  const rotaPagInicial = "/animal";
  const [formValues, setFormValues] = useState(undefined);
  const [isEnabled, setEnabled] = useState(false);
  const dispatch = useDispatch();
  const { animal } = useSelector(store => store.animalStore);
  const { baiasAll } = useSelector(store => store.baiaStore);
  const { racasAll } = useSelector(store => store.racaStore);
  const { fichaAnimalsAll } = useSelector(store => store.fichaAnimalStore);

  const esquemaValidacao = Yup.object({
    nome: Yup.string().required("O nome é obrigatório."),
    raca: Yup.object({
      id: Yup.string().required("A raca é obrigatória.")
    })
  });

  useEffect(() => {
    let dto = Object.assign({}, animal);
    dto.dtNascimento =
      animal.dtNascimento !== undefined
        ? moment(animal.dtNascimento).format("YYYY-MM-DD")
        : undefined;
    dto.dtEntrada =
      animal.dtEntrada !== undefined
        ? moment(animal.dtEntrada).format("YYYY-MM-DD")
        : undefined;
    setFormValues(dto);
  }, [animal]);

  useEffect(() => {
    dispatch(listarRel(props.match.params.id));
    dispatch(listarTodos());
    dispatch(listRacaAll());
    if (props.match.params && props.match.params.id) {
      dispatch(get(props.match.params.id));
    } else {
      dispatch(cleanRegister());
      setEnabled(true);
    }
    dispatch(cleanMessages());
  }, []);

  const toggleEnabled = () => {
    setEnabled(!isEnabled);
  };

  const submitForm = async values => {
    try {
      let dto = Object.assign({}, values);
      dto.dtNascimento =
        values.dtNascimento !== null
          ? moment(values.dtNascimento, moment.ISO_8601)
          : null;
      dto.dtEntrada =
        values.dtEntrada !== null
          ? moment(values.dtEntrada, moment.ISO_8601)
          : null;
      setFormValues(dto);
      let isUpdate = dto.id > 0;
      let result = isUpdate
        ? await dispatch(atualizar(dto))
        : await dispatch(criar(dto));
      if (
        result.action.payload.status === 200 ||
        result.action.payload.status === 201
      ) {
        props.history.push(rotaPagInicial);
        toast.success(
          <AlertList message={result.action.payload.headers["x-app-alert"]} />
        );
      }
    } catch (error) {
      toast.error(
        <AlertList message={error.response.headers["x-app-error"]} />
      );
    }
  };

  const footer = (
    <CardFooter>
      {isEnabled && (
        <React.Fragment>
          <Button color="primary">Salvar</Button>
          <Link to={rotaPagInicial} className="ml-1">
            <Button>Cancelar</Button>
          </Link>
        </React.Fragment>
      )}
      {!isEnabled && (
        <React.Fragment>
          <Button color="primary" onClick={toggleEnabled}>
            Editar
          </Button>
          <Link to={rotaPagInicial} className="ml-1">
            <Button>Voltar</Button>
          </Link>
          <Link to={`/animal/rel/${animal.id}`}>
            <Button
              className="float-right btn btn-warning"
              disabled={fichaAnimalsAll.length !== 0 ? false : true}
              color="primary"
            >
              Imprimir Fichas
            </Button>
          </Link>
        </React.Fragment>
      )}
    </CardFooter>
  );

  const Formulario = () => (
    <FinalForm
      onSubmit={submitForm}
      initialValues={formValues}
      validate={values => validate(values, esquemaValidacao)}
      render={({ handleSubmit, pristine, submitForm, values, meta }) => (
        <Form onSubmit={handleSubmit} name="simpleForm">
          <Field name="id" type="hidden">
            {({ input, meta }) => <Input {...input} />}
          </Field>
          <CardBody>
            <Row>
              <Col>
                <Field name="nome">
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">Nome</span>
                      <Input
                        type="text"
                        {...input}
                        placeholder="Nome do animal"
                        invalid={meta.invalid && meta.touched}
                        disabled={!isEnabled}
                      />
                      <FormFeedback>{meta.error}</FormFeedback>
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field type="checkbox" name="status">
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <div className="form-check">
                        <Input
                          type="checkbox"
                          className="form-check-input"
                          {...input}
                          value="true"
                          invalid={meta.invalid && meta.touched}
                          disabled={!isEnabled}
                        />
                        <label
                          className="form-check-label"
                          htmlFor="defaultCheck1"
                        >
                          <span className="badge badge-danger">Inativo</span>
                        </label>
                      </div>
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field type="date" name="dtNascimento">
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">
                        Data de nascimento
                      </span>
                      <Input
                        type="date"
                        {...input}
                        placeholder="Data de Nascimento"
                        disabled={!isEnabled}
                      />
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field type="date" name="dtEntrada">
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">Data de entrada</span>
                      <Input
                        type="date"
                        {...input}
                        placeholder="Data de Nascimento"
                        disabled={!isEnabled}
                      />
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field type="select" name="baia.id" component>
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">Baia do animal</span>
                      <Input
                        type="select"
                        {...input}
                        invalid={meta.invalid && meta.touched}
                        disabled={!isEnabled}
                      >
                        <option>Escolha uma baia...</option>
                        {baiasAll &&
                          baiasAll.map(it => (
                            <option key={`opt-${it.id}`} value={it.id}>
                              {it.nome}
                            </option>
                          ))}
                      </Input>
                      <FormFeedback>{meta.error}</FormFeedback>
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field type="select" name="raca.id" component>
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">Raça do animal</span>
                      <Input
                        type="select"
                        {...input}
                        invalid={meta.invalid && meta.touched}
                        disabled={!isEnabled}
                      >
                        <option>Escolha uma raça...</option>
                        {racasAll &&
                          racasAll.map(it => (
                            <option key={`opt-${it.id}`} value={it.id}>
                              {it.nome}
                            </option>
                          ))}
                      </Input>
                      <FormFeedback>{meta.error}</FormFeedback>
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
          </CardBody>
          {footer}
        </Form>
      )}
    />
  );

  return (
    <div>
      <Row>
        <Col sm="6" className="m-auto">
          <Card>
            <CardHeader>Animal</CardHeader>
            <Formulario />
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default TipoContaInsertEdit;
