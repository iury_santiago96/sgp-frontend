import React, { useEffect, useState } from "react";
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Form,
  FormFeedback,
  Input,
  InputGroup,
  Row
} from "reactstrap";
import { Field, Form as FinalForm } from "react-final-form";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import {
  atualizar,
  cleanMessages,
  cleanRegister,
  criar,
  get
} from "../../reducers/vacina-reducer";
import { listarTodos } from "../../reducers/tipo-vacina-reducer";
import * as Yup from "yup";
import { validate } from "../../util/validation-util";
import moment from "moment";
import { toast } from "react-toastify";
import AlertList from "../../containers/AlertList/AlertList";

const TipoContaInsertEdit = props => {
  const rotaPagInicial = "/vacina";
  const [formValues, setFormValues] = useState(undefined);
  const [isEnabled, setEnabled] = useState(false);
  const dispatch = useDispatch();
  const { vacina } = useSelector(store => store.vacinaStore);
  const { tipoVacinasAll } = useSelector(store => store.tipoVacinaStore);

  const esquemaValidacao = Yup.object({
    nome: Yup.string().required("O nome é obrigatório."),
    marca: Yup.string().required("A marca é obrigatória."),
    lote: Yup.string().required("O Lote é obrigatório."),
    fabricante: Yup.string().required("O fabricante é obrigatório."),
    validade: Yup.date().required("A validade é obrigatória."),
    tipoVacina: Yup.object({
      id: Yup.string().required("Tipo de vacina é obrigatório.")
    }),
    quantidadeAplicacao: Yup.date().required(
      "A Quantidade de aplicações é obrigatória."
    )
  });

  useEffect(() => {
    let dto = Object.assign({}, vacina);
    dto.validade =
      vacina.validade !== undefined
        ? moment(vacina.validade).format("YYYY-MM-DD")
        : undefined;
    setFormValues(dto);
  }, [vacina]);

  useEffect(() => {
    dispatch(listarTodos());
    if (props.match.params && props.match.params.id) {
      dispatch(get(props.match.params.id));
    } else {
      dispatch(cleanRegister());
      setEnabled(true);
    }
    dispatch(cleanMessages());
  }, []);

  const toggleEnabled = () => {
    setEnabled(!isEnabled);
  };

  const submitForm = async values => {
    try {
      let dto = Object.assign({}, values);
      dto.validade =
        values.validade !== null
          ? moment(values.validade, moment.ISO_8601)
          : null;
      setFormValues(dto);
      let isUpdate = dto.id > 0;
      let result = isUpdate
        ? await dispatch(atualizar(dto))
        : await dispatch(criar(dto));
      if (
        result.action.payload.status === 200 ||
        result.action.payload.status === 201
      ) {
        props.history.push(rotaPagInicial);
        toast.success(
          <AlertList message={result.action.payload.headers["x-app-alert"]} />
        );
      }
    } catch (error) {
      toast.error(
        <AlertList message={error.response.headers["x-app-error"]} />
      );
    }
  };

  const footer = (
    <CardFooter>
      {isEnabled && (
        <React.Fragment>
          <Button color="primary">Salvar</Button>
          <Link to={rotaPagInicial} className="ml-1">
            <Button>Cancelar</Button>
          </Link>
        </React.Fragment>
      )}
      {!isEnabled && (
        <React.Fragment>
          <Button color="primary" onClick={toggleEnabled}>
            Editar
          </Button>
          <Link to={rotaPagInicial} className="ml-1">
            <Button>Voltar</Button>
          </Link>
        </React.Fragment>
      )}
    </CardFooter>
  );

  const Formulario = () => (
    <FinalForm
      onSubmit={submitForm}
      initialValues={formValues}
      validate={values => validate(values, esquemaValidacao)}
      render={({ handleSubmit, pristine, submitForm, values, meta }) => (
        <Form onSubmit={handleSubmit} name="simpleForm">
          <Field name="id" type="hidden">
            {({ input, meta }) => <Input {...input} />}
          </Field>
          <CardBody>
            <Row>
              <Col>
                <Field name="nome">
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">Nome</span>
                      <Input
                        type="text"
                        {...input}
                        placeholder="nome da vacina"
                        invalid={meta.invalid && meta.touched}
                        disabled={!isEnabled}
                      />
                      <FormFeedback>{meta.error}</FormFeedback>
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field name="marca">
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">Marca</span>
                      <Input
                        type="text"
                        {...input}
                        placeholder="Marca da vacina"
                        invalid={meta.invalid && meta.touched}
                        disabled={!isEnabled}
                      />
                      <FormFeedback>{meta.error}</FormFeedback>
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field name="lote">
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">Lote</span>
                      <Input
                        type="text"
                        {...input}
                        placeholder="Lote da vacina"
                        invalid={meta.invalid && meta.touched}
                        disabled={!isEnabled}
                      />
                      <FormFeedback>{meta.error}</FormFeedback>
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field name="fabricante">
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">Fabricante</span>
                      <Input
                        type="text"
                        {...input}
                        placeholder="Fabricante da vacina"
                        invalid={meta.invalid && meta.touched}
                        disabled={!isEnabled}
                      />
                      <FormFeedback>{meta.error}</FormFeedback>
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field type="date" name="validade">
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">Validade</span>
                      <Input
                        type="date"
                        {...input}
                        placeholder="Data de validade"
                        invalid={meta.invalid && meta.touched}
                        disabled={!isEnabled}
                      />
                      <FormFeedback>{meta.error}</FormFeedback>
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field type="number" name="quantidadeAplicacao" component>
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">
                        Quantidade de Aplicações
                      </span>
                      <Input
                        type="number"
                        {...input}
                        placeholder="Quantidade de Aplicações da vacina"
                        invalid={meta.invalid && meta.touched}
                        disabled={!isEnabled}
                      />
                      <FormFeedback>{meta.error}</FormFeedback>
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field type="number" name="intervaloDeDias" component>
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">
                        Intervalo de aplicação
                      </span>
                      <Input
                        type="number"
                        {...input}
                        placeholder="intervalo de aplicação da vacina"
                        invalid={meta.invalid && meta.touched}
                        disabled={!isEnabled}
                      />
                      <FormFeedback>{meta.error}</FormFeedback>
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field type="select" name="tipoVacina.id" component>
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">Tipo de vacina</span>
                      <Input
                        type="select"
                        {...input}
                        invalid={meta.invalid && meta.touched}
                        disabled={!isEnabled}
                      >
                        <option>Escolha um tipo de vacina...</option>
                        {tipoVacinasAll &&
                          tipoVacinasAll.map(it => (
                            <option key={`opt-${it.id}`} value={it.id}>
                              {it.tipo}
                            </option>
                          ))}
                      </Input>
                      <FormFeedback>{meta.error}</FormFeedback>
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
          </CardBody>
          {footer}
        </Form>
      )}
    />
  );

  return (
    <div>
      <Row>
        <Col sm="6" className="m-auto">
          <Card>
            <CardHeader>Vacina</CardHeader>
            <Formulario />
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default TipoContaInsertEdit;
