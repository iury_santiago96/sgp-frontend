import React, { useEffect, useState } from "react";
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Form,
  FormFeedback,
  Input,
  InputGroup,
  Row
} from "reactstrap";
import { Field, Form as FinalForm } from "react-final-form";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import {
  atualizar,
  cleanMessages,
  cleanRegister,
  criar,
  get
} from "../../reducers/conta-reducer";
import { listarTodos } from "../../reducers/tipo-conta-reducer";
import { listarTodos as listVoluntarioAll } from "../../reducers/voluntario-reducer";
import * as Yup from "yup";
import { validate } from "../../util/validation-util";
import moment from "moment";
import { toast } from "react-toastify";
import AlertList from "../../containers/AlertList/AlertList";

const TipoContaInsertEdit = props => {
  const rotaPagInicial = "/conta";
  const [formValues, setFormValues] = useState(undefined);
  const [isEnabled, setEnabled] = useState(false);
  const dispatch = useDispatch();
  const { conta } = useSelector(store => store.contaStore);
  const { tipoContasAll } = useSelector(store => store.tipoContaStore);
  const { voluntariosAll } = useSelector(store => store.voluntarioStore);

  const esquemaValidacao = Yup.object({
    nome: Yup.string().required("O nome é obrigatório."),
    valor: Yup.number().required("O valor é obrigatório."),
    dtConta: Yup.date().required("A data é obrigatória."),
    voluntario: Yup.object({
      id: Yup.string().required("O voluntário é obrigatório.")
    }),
    tipoConta: Yup.object({
      id: Yup.string().required("O voluntário é obrigatório.")
    })
  });

  useEffect(() => {
    let dto = Object.assign({}, conta);
    dto.dtConta =
      conta.dtConta !== undefined
        ? moment(conta.dtConta).format("YYYY-MM-DD")
        : undefined;
    setFormValues(dto);
  }, [conta]);

  useEffect(() => {
    dispatch(listarTodos());
    dispatch(listVoluntarioAll());
    if (props.match.params && props.match.params.id) {
      dispatch(get(props.match.params.id));
    } else {
      dispatch(cleanRegister());
      setEnabled(true);
    }
    dispatch(cleanMessages());
  }, []);

  const toggleEnabled = () => {
    setEnabled(!isEnabled);
  };

  const submitForm = async values => {
    try {
      let dto = Object.assign({}, values);
      dto.dtConta =
        values.dtConta !== null
          ? moment(values.dtConta, moment.ISO_8601)
          : null;
      setFormValues(dto);
      let isUpdate = dto.id > 0;
      let result = isUpdate
        ? await dispatch(atualizar(dto))
        : await dispatch(criar(dto));
      if (
        result.action.payload.status === 200 ||
        result.action.payload.status === 201
      ) {
        props.history.push(rotaPagInicial);
        toast.success(
          <AlertList message={result.action.payload.headers["x-app-alert"]} />
        );
      }
    } catch (error) {
      toast.error(
        <AlertList message={error.response.headers["x-app-error"]} />
      );
    }
  };

  const footer = (
    <CardFooter>
      {isEnabled && (
        <React.Fragment>
          <Button color="primary">Salvar</Button>
          <Link to={rotaPagInicial} className="ml-1">
            <Button>Cancelar</Button>
          </Link>
        </React.Fragment>
      )}
      {!isEnabled && (
        <React.Fragment>
          <Button color="primary" onClick={toggleEnabled}>
            Editar
          </Button>
          <Link to={rotaPagInicial} className="ml-1">
            <Button>Voltar</Button>
          </Link>
        </React.Fragment>
      )}
    </CardFooter>
  );

  const Formulario = () => (
    <FinalForm
      onSubmit={submitForm}
      initialValues={formValues}
      validate={values => validate(values, esquemaValidacao)}
      render={({ handleSubmit, pristine, submitForm, values, meta }) => (
        <Form onSubmit={handleSubmit} name="simpleForm">
          <Field name="id" type="hidden">
            {({ input, meta }) => <Input {...input} />}
          </Field>
          <CardBody>
            <Row>
              <Col>
                <Field name="nome">
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">Nome</span>
                      <Input
                        type="text"
                        {...input}
                        placeholder="nome da conta"
                        invalid={meta.invalid && meta.touched}
                        disabled={!isEnabled}
                      />
                      <FormFeedback>{meta.error}</FormFeedback>
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field type="number" name="valor" component>
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">Valor</span>
                      <Input
                        type="number"
                        {...input}
                        placeholder="Valor da conta"
                        invalid={meta.invalid && meta.touched}
                        disabled={!isEnabled}
                      />
                      <FormFeedback>{meta.error}</FormFeedback>
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field type="date" name="dtConta" component>
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">Data da conta</span>
                      <Input
                        type="date"
                        {...input}
                        placeholder="Data da conta"
                        invalid={meta.invalid && meta.touched}
                        disabled={!isEnabled}
                      />
                      <FormFeedback>{meta.error}</FormFeedback>
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field type="select" name="tipoConta.id" component>
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">Tipo de conta</span>
                      <Input
                        type="select"
                        {...input}
                        invalid={meta.invalid && meta.touched}
                        disabled={!isEnabled}
                      >
                        <option>Escolha um tipo de conta...</option>
                        {tipoContasAll &&
                          tipoContasAll.map(it => (
                            <option key={`opt-${it.id}`} value={it.id}>
                              {it.tipo}
                            </option>
                          ))}
                      </Input>
                      <FormFeedback>{meta.error}</FormFeedback>
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field type="select" name="voluntario.id" component>
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">Voluntario</span>
                      <Input
                        type="select"
                        {...input}
                        invalid={meta.invalid && meta.touched}
                        disabled={!isEnabled}
                      >
                        <option>Escolha um Voluntario...</option>
                        {voluntariosAll &&
                          voluntariosAll.map(it => (
                            <option key={`opt-${it.id}`} value={it.id}>
                              {it.nome}
                            </option>
                          ))}
                      </Input>
                      <FormFeedback>{meta.error}</FormFeedback>
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
          </CardBody>
          {footer}
        </Form>
      )}
    />
  );

  return (
    <div>
      <Row>
        <Col sm="6" className="m-auto">
          <Card>
            <CardHeader>Conta</CardHeader>
            <Formulario />
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default TipoContaInsertEdit;
