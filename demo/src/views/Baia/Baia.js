import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { listar, searchRegister, excluir } from "../../reducers/Baia-reducer";
import DeleteModal from "../../containers/DeleteModal/DeleteModal";
import { Card, CardFooter, CardHeader } from "reactstrap";
import Pagination from "react-js-pagination";
import * as lodash from "lodash";

const Baia = props => {
  const RotaPagInsert = "/baia/add";
  const dispatch = useDispatch();
  const [termo, setTermo] = useState("");
  const [baiasId, setbaiasId] = useState(false);
  const [modalDeleteVisible, setModalDeleteVisible] = useState(false);
  const { loading, baias } = useSelector(state => state.baiaStore);

  useEffect(() => {
    dispatch(listar());
  }, []);

  const onChangeSearchInput = evt => {
    debouncedSearch(evt.target.value);
  };

  const debouncedSearch = lodash.debounce(function(query) {
    setTermo(query);
    if (query && query !== "") {
      dispatch(searchRegister(query));
    } else {
      dispatch(listar());
    }
  }, 500);

  const modalDeleteVisibleToggle = () => {
    setModalDeleteVisible(!modalDeleteVisible);
  };

  const openModalDelete = (id, e) => {
    setbaiasId(id);
    setModalDeleteVisible(true);
  };

  function handlePageChange(pageNumber) {
    if (termo && termo !== "") {
      dispatch(searchRegister(termo, pageNumber - 1));
    } else {
      dispatch(listar(pageNumber - 1));
    }
  }

  const cardHeader = () => {
    return (
      <CardHeader>
        <nav className="navbar navbar-light bg-light">
          <form className="form-inline w-75">
            <input
              className="form-control mr-sm-2 w-25"
              onChange={onChangeSearchInput}
              type="search"
              placeholder="Procurar..."
              aria-label="Search"
            />
          </form>
          <Link to={RotaPagInsert}>
            <button type="button" className="btn btn-primary float-right">
              Inserir
            </button>
          </Link>
          {loading && (
            <div className="d-inline p-2 bg-primary text-white">
              <span>Carregando....</span>
            </div>
          )}
        </nav>
      </CardHeader>
    );
  };

  const elementoTipoVol = tipo => {
    const rotaPagEditView = `/baia/edit/${tipo.id}`;
    return (
      <tr key={tipo.id} className="text-center">
        <td>{tipo.nome}</td>
        <td>{tipo.capacidade}</td>
        <td>
          <Link to={rotaPagEditView}>
            <button type="button" className="btn btn-primary mr-1">
              Visualizar
            </button>
          </Link>
          <button
            type="button"
            className="btn btn-danger"
            onClick={openModalDelete.bind(null, tipo.id)}
          >
            Deletar
          </button>
        </td>
      </tr>
    );
  };

  const cardFooter = () => {
    return (
      <CardFooter>
        <Pagination
          innerClass="pagination justify-content-center"
          itemClass="page-item"
          linkClass="page-link"
          activePage={baias.number + 1}
          itemsCountPerPage={baias.pageable.pageSize}
          totalItemsCount={baias.totalElements}
          onChange={handlePageChange.bind(this)}
        />
      </CardFooter>
    );
  };

  return (
    <div>
      <Card>
        {cardHeader()}
        <table className="table table-striped mb-0 ">
          <thead>
            <tr className="text-center">
              <th scope="col">Nome</th>
              <th scope="col">Capacidade</th>
              <th scope="col">Ações</th>
            </tr>
          </thead>
          <tbody>
            {baias &&
              baias.content &&
              baias.content.map(it => elementoTipoVol(it))}
          </tbody>
        </table>
        {baias && baias.content && cardFooter(baias.pageable.number)}
        <DeleteModal
          isOpen={modalDeleteVisible}
          toggle={modalDeleteVisibleToggle}
          idRegistro={baiasId}
          deleteReducer={excluir}
        />
      </Card>
    </div>
  );
};

export default Baia;
