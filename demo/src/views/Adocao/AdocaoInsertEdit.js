import React, { useEffect, useState } from "react";
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  FormFeedback,
  Col,
  Form,
  Input,
  InputGroup,
  Row
} from "reactstrap";
import { Field, Form as FinalForm } from "react-final-form";
import arrayMutators from "final-form-arrays";
import { FieldArray } from "react-final-form-arrays";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import {
  atualizar,
  cleanMessages,
  cleanRegister,
  criar,
  get
} from "../../reducers/adocao-reducer";
import { listarTodos } from "../../reducers/voluntario-reducer";
import { listarTodos as listAdotanteAll } from "../../reducers/adotante-reducer";
import { listarAtivos as listAnimalAll } from "../../reducers/animal-reducer";
import * as Yup from "yup";
import { validate } from "../../util/validation-util";
import moment from "moment";
import { toast } from "react-toastify";
import AlertList from "../../containers/AlertList/AlertList";

const TipoContaInsertEdit = props => {
  const rotaPagInicial = "/adocao";
  const [formValues, setFormValues] = useState(undefined);
  const [isEnabled, setEnabled] = useState(false);
  const dispatch = useDispatch();
  const { adocao } = useSelector(store => store.adocaoStore);
  const { voluntariosAll } = useSelector(store => store.voluntarioStore);
  const { animalsAll } = useSelector(store => store.animalStore);
  const { adotantesAll } = useSelector(store => store.adotanteStore);

  const esquemaValidacao = Yup.object({
    dtAdocao: Yup.date().required("A data é necessária"),
    voluntario: Yup.object({
      id: Yup.string().required("O voluntário é obrigatório.")
    }),
    adotante: Yup.object({
      id: Yup.string().required("O adotante é obrigatório.")
    }),
    animais: Yup.object({
      id: Yup.string().required("O adotante é obrigatório.")
    })
  });

  useEffect(() => {
    let dto = Object.assign({}, adocao);
    dto.dtAdocao =
      adocao.dtAdocao !== undefined
        ? moment(adocao.dtAdocao).format("YYYY-MM-DD")
        : undefined;
    setFormValues(dto);
  }, [adocao]);

  useEffect(() => {
    dispatch(listarTodos());
    dispatch(listAdotanteAll());
    dispatch(listAnimalAll());
    if (props.match.params && props.match.params.id) {
      dispatch(get(props.match.params.id));
    } else {
      dispatch(cleanRegister());
      setEnabled(true);
    }
    dispatch(cleanMessages());
  }, []);

  const toggleEnabled = () => {
    setEnabled(!isEnabled);
  };

  const submitForm = async values => {
    try {
      let dto = Object.assign({}, values);
      dto.dtAdocao =
        values.dtAdocao !== null
          ? moment(values.dtAdocao, moment.ISO_8601)
          : null;
      setFormValues(dto);
      let isUpdate = dto.id > 0;
      let result = isUpdate
        ? await dispatch(atualizar(dto))
        : await dispatch(criar(dto));
      if (
        result.action.payload.status === 200 ||
        result.action.payload.status === 201
      ) {
        props.history.push(rotaPagInicial);
        toast.success(
          <AlertList message={result.action.payload.headers["x-app-alert"]} />
        );
      }
    } catch (error) {
      toast.error(
        <AlertList message={error.response.headers["x-app-error"]} />
      );
    }
  };

  const footer = (
    <CardFooter>
      {isEnabled && (
        <React.Fragment>
          <Button color="primary">Salvar</Button>
          <Link to={rotaPagInicial} className="ml-1">
            <Button>Cancelar</Button>
          </Link>
        </React.Fragment>
      )}
      {!isEnabled && (
        <React.Fragment>
          <Button color="primary" onClick={toggleEnabled}>
            Editar
          </Button>
          <Link to={rotaPagInicial} className="ml-1">
            <Button>Voltar</Button>
          </Link>
        </React.Fragment>
      )}
    </CardFooter>
  );

  const Formulario = () => (
    <FinalForm
      onSubmit={submitForm}
      initialValues={formValues}
      validate={values => validate(values, esquemaValidacao)}
      mutators={{
        ...arrayMutators
      }}
      render={({
        handleSubmit,
        pristine,
        submitForm,
        values,
        meta,
        form: {
          mutators: { push, pop }
        }
      }) => (
        <Form onSubmit={handleSubmit} name="simpleForm">
          <Field name="id" type="hidden">
            {({ input, meta }) => <Input {...input} />}
          </Field>
          <CardBody>
            <Row>
              <Col>
                <Field type="date" name="dtAdocao">
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">Data da Adoção</span>
                      <Input
                        type="date"
                        {...input}
                        placeholder="Data da Adoção"
                        invalid={meta.invalid && meta.touched}
                        disabled={!isEnabled}
                      />
                      <FormFeedback>{meta.error}</FormFeedback>
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field type="select" name="voluntario.id" component>
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">Voluntário</span>
                      <Input
                        type="select"
                        {...input}
                        invalid={meta.invalid && meta.touched}
                        disabled={!isEnabled}
                      >
                        <option>Escolha uma voluntário...</option>
                        {voluntariosAll &&
                          voluntariosAll.map(it => (
                            <option key={`opt-${it.id}`} value={it.id}>
                              {it.nome}
                            </option>
                          ))}
                      </Input>
                      <FormFeedback>{meta.error}</FormFeedback>
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field type="select" name="adotante.id" component>
                  {({ input, meta }) => (
                    <InputGroup className="mb-2">
                      <span className="input-group-text">Adotante</span>
                      <Input
                        type="select"
                        {...input}
                        invalid={meta.invalid && meta.touched}
                        disabled={!isEnabled}
                      >
                        <option>Escolha um adotante...</option>
                        {adotantesAll &&
                          adotantesAll.map(it => (
                            <option key={`opt-${it.id}`} value={it.id}>
                              {it.nome}
                            </option>
                          ))}
                      </Input>
                      <FormFeedback>{meta.error}</FormFeedback>
                    </InputGroup>
                  )}
                </Field>
              </Col>
            </Row>
            <hr />
            <div className="d-flex justify-content-center mt-2 mb-3">
              <button
                className="btn btn-success mr-1"
                type="button"
                disabled={!isEnabled}
                onClick={() => push("animais", undefined)}
              >
                Adicionar Animal
              </button>
              <button
                className="btn btn-danger"
                type="button"
                disabled={!isEnabled}
                onClick={() => pop("animais")}
              >
                Remover Animal
              </button>
            </div>
            <Row>
              <Col>
                <div>
                  <FieldArray name="animais">
                    {({ fields }) =>
                      fields.map((name, index) => (
                        <div key={name}>
                          <Row>
                            <label hidden>Cust. #{index + 1}</label>
                            <Col>
                              <Field
                                type="select"
                                name={`${name}.id`}
                                component
                              >
                                {({ input, meta }) => (
                                  <InputGroup className="mb-2">
                                    <span className="input-group-text">
                                      Animal {index + 1}
                                    </span>
                                    <Input
                                      type="select"
                                      {...input}
                                      invalid={meta.invalid && meta.touched}
                                      disabled={!isEnabled}
                                    >
                                      <option>Escolha um Animal...</option>
                                      {animalsAll &&
                                        animalsAll.map(it => (
                                          <option
                                            key={`opt-${it.id}`}
                                            value={it.id}
                                          >
                                            {it.nome}
                                          </option>
                                        ))}
                                    </Input>
                                    <div className="input-group-append">
                                      <button
                                        className="btn btn-outline-secondary"
                                        type="button"
                                        disabled={!isEnabled}
                                        onClick={() => fields.remove(index)}
                                      >
                                        ❌
                                      </button>
                                    </div>
                                    <FormFeedback>{meta.error}</FormFeedback>
                                  </InputGroup>
                                )}
                              </Field>
                            </Col>
                          </Row>
                        </div>
                      ))
                    }
                  </FieldArray>
                </div>
              </Col>
            </Row>
            <hr />
          </CardBody>
          {footer}
        </Form>
      )}
    />
  );

  return (
    <div>
      <Row>
        <Col sm="6" className="m-auto">
          <Card>
            <CardHeader>Adoção</CardHeader>
            <Formulario />
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default TipoContaInsertEdit;
