import './polyfill'
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import initStore from './config/store'
import {Provider} from 'react-redux';
// import registerServiceWorker from './registerServiceWorker';

const store = initStore();

ReactDOM.render(<Provider store={store}><App/></Provider>, document.querySelector('#demo'));
// ReactDOM.render(<App />, document.getElementById('root'));
// registerServiceWorker();
