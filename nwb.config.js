module.exports = {
  type: "react-component",
  npm: {
    esModules: true,
    umd: {
      global: "CoreUIReact",
      externals: {
        react: "React"
      }
    }
  },
  webpack: {
    html: {
      template: "demo/src/index.html",
      title: "SGP"
    }
  }
};
